package dnsbroker_test

import (
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/davecgh/go-spew/spew"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadConfig(t *testing.T) {

	var config dnsbroker.DnstrackConfig
	if _, err := toml.DecodeFile("dnstrack.toml", &config); err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("version: %s\n", config.Version)

	//fmt.Printf("blacklist_prefix: %s\n", config.BlacklistPrefix)
	fmt.Printf("blacklist_ttl: %d\n", config.BlacklistTTL)
	spew.Dump(config.WhiteList)

	fmt.Printf("backend.MasterUrl: %s\n", config.Backend.MasterUrl)
	fmt.Printf("backend.PrometheusUrl: %s\n", config.Backend.PrometheusUrl)

	fmt.Printf("redis.url: %s\n", config.Redis.Url)

	fmt.Printf("influxdb.url: %s\n", config.Influxdb.Url)
	fmt.Printf("influxdb.db: %s\n", config.Influxdb.Db)
	fmt.Printf("influxdb.username: %s\n", config.Influxdb.Username)
	fmt.Printf("influxdb.password: %s\n", config.Influxdb.Password)
	fmt.Printf("influxdb.flush_delay: %d\n", config.Influxdb.FlushDelay)

	fmt.Printf("dnswatch.scan_interval: %d\n", config.Dnswatch.ScanInterval)
	fmt.Printf("dnswatch.scan_period: %d\n", config.Dnswatch.ScanPeriod)
	fmt.Printf("dnswatch.scan_limit: %d\n", config.Dnswatch.ScanLimit)

	fmt.Printf("dnsguard.name: %s\n", config.DnsGuard.Name)
	fmt.Printf("dnsguard.question_channel: %s\n", config.DnsGuard.QuestionChannel)
	fmt.Printf("dnsguard.drop_channel: %s\n", config.DnsGuard.DropChannel)
	fmt.Printf("dnsguard.query_channel: %s\n", config.DnsGuard.QueryChannel)

	spew.Dump(config.DnsGuard.ActiveChannels)
}

func TestNewConfig(t *testing.T) {

	// create new config
	conf := dnsbroker.NewConfig("./dnstrack.toml")
	fmt.Printf("version: %s\n", conf.Version)
	spew.Dump(conf)

	// get a copy of the config
	conf2 := dnsbroker.GetConfig()
	fmt.Printf("version: %s\n", conf2.Version)
	spew.Dump(conf2)
	assert.Equal(t, conf, conf2)

	// modify the configuration copy
	conf2.Version = "0.2"

	// the original configuration should not be affected
	assert.NotEqual(t, conf.Version, conf2.Version)
	fmt.Printf("version: %s\n", conf.Version)

}

func TestBackendConfig(t *testing.T) {

	// create new config
	conf := dnsbroker.NewConfig("./dnstrack.toml")
	//fmt.Printf("version: %s\n", conf.Version)
	//spew.Dump(conf)

	// get a copy of the config
	conf2 := dnsbroker.GetConfig()
	assert.Equal(t, conf, conf2)

	fmt.Printf("version: %s\n", conf2.Version)
	spew.Dump(conf2)
	assert.Equal(t, "0.1", conf2.Version)

	instance := conf2.Backend.ProbeId
	if instance == "demo" {

		job := conf2.Backend.Job
		assert.Equal(t, "dnsguard", job)

		master_url := conf2.Backend.MasterUrl
		assert.Equal(t, "http://localhost:3333/api/v1/agents/demo/config", master_url)

		prometheus_url := conf2.Backend.PrometheusUrl
		assert.Equal(t, "http://localhost:9091/metrics/job/dnsguard/instance/demo", prometheus_url)

		config_timer := conf2.Backend.PollConfigFrequency
		assert.Equal(t, 86400, config_timer)

	}
}
