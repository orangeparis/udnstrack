/*

	a store based on redis

	set domains to blacklist
	get domains that are black listed


   api to store

   Domain Counter:
       IncrDomainCounter( domain , info )
       GetDomainCounter( domain )
       ClearDomainCounter(domain )
       ClearAllDomainCounter()
   Domain Blacklist
      CheckBlacklist( domain )
      AddBlacklist( domain , ttl )
      ClearBlacklist( domain )
      ClearAllBlacklist()
   Domain Whitelist
      CheckWhitelist()
      AddWhitelist( domain )
      ClearWhitelist( domain )

	Rules  ( TXT , WKS , NULL )



	interface to store metrics

		NOTE: use global variable Metric * DnsguardMetrics from "bitbucket.org/orangeparis/udnstrack/metrics"
			so it has to be initialized before

		PublishQuestion(id ,event)

*/

package dnsbroker

import (
	"bitbucket.org/orangeparis/udnstrack/metrics"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"log"
	"reflect"
	"strconv"
	//"github.com/davecgh/go-spew/spew"
)

const (

	// prefix for keys to store data in redis database
	blacklist_prefix = "blacklist"
	whitelist_prefix = "whitelist"
	counter_prefix   = "hit"
	drop_prefix      = "drop"
	rule_prefix      = "rule"

	parameters_prefix = "parameters"

	drop_ttl = 3600
)

var (
	default_blacklist_ttl = 3600 * 24
)

type Store struct {
	RedisClient
}

func NewStore() *Store {

	client := NewRedisClient("store")

	store := Store{*client}

	// TODO: check here the metrics.Metric has been initialized or panic

	return &store

}

func (store *Store) Close() error {
	//
	return nil
}

func (store *Store) Lock() {
	//
	store.RedisClient.Lock()
}

func (store *Store) Unlock() {
	//
	store.RedisClient.Unlock()
}

//
//   domain counter  hit:<domain>
//

func (store *Store) IncrDomainCounter(domain string, info string) error {
	// increment counter for this domain  , hit:<domain>

	store.Lock()
	defer store.Unlock()

	// distinct domains counter
	// in fact domain is the etlp_p1 and info is the domain  hits:<etlp_p1>
	etld_key := counter_prefix + "s:" + domain
	n, err := store.Conn.Do("HSET", etld_key, info, "")
	_ = n
	// hit counter
	// key := store.hitKey(domain)
	//n,err = store.Conn.Do("INCR" ,key)

	if err != nil {
		log.Printf("fail to increment domain counter :%s (%s) ", domain, err)
	} else {
		// set OK: try expire
		//log.Printf("hit counter incremented for domain: %s ==> %d" , domain,n)
	}
	return err
}

func (store *Store) GetDomainCounter(domain string) uint64 {
	// get counter (int) for this  , hit:<domain>
	// return counter value or 0
	var counter int64 = 0
	store.Lock()
	defer store.Unlock()

	//
	etld_key := counter_prefix + "s:" + domain
	counter, _ = redis.Int64(store.Conn.Do("HLEN", etld_key))

	// //key := store.hitKey(domain)
	//v,err := store.Conn.Do("GET" ,string(key))
	//if err != nil {
	//	log.Printf("fail to get domain count :%s ", err)
	//
	//} else {
	//	//
	//	//i := strconv.Atoi(v.(string))
	//	if v == nil {
	//		v= "0"
	//	}
	//	if counter, err = strconv.ParseInt(string(v.([]uint8)), 10, 64); err == nil {
	//		fmt.Printf("counter =%d, type: %T\n", counter, counter)
	//	} else {
	//		fmt.Printf("cannot convert redis counter value: %s" , v)
	//	}
	//}
	return uint64(counter)
}

func (store *Store) ClearDomainCounter(domain string) error {
	// del counter for this domain , hit:<domain>
	// return error

	store.Lock()
	defer store.Unlock()

	etld_key := counter_prefix + "s:" + domain
	_, err := store.Conn.Do("DEL", etld_key)

	//key := store.hitKey(domain)
	//_,err := store.Conn.Do("DEL" ,key)

	if err != nil {
		log.Printf("fail to delete domain counter :%s ", err)

	} else {
		//
		//log.Printf("counter deleted for domain: %s" , domain)
	}
	return err
}

func (store *Store) ListDomainCounter() ([]string, error) {
	//
	// return list of all domain counter keys : hit:*
	//
	prefix := counter_prefix + "s"
	counter_keys, err := store.Keys(prefix)
	return counter_keys, err
}

//
//   blacklist
//

func (store *Store) AddBlacklist(domain string, ttl uint64) error {
	// add a domain to the blacklist

	return store.AddToList(blacklist_prefix, domain, ttl)

}

func (store *Store) IsInBlacklist(domain string) bool {
	// check domain return False if domain is in blacklist
	// True if not in blacklist or cannot read db

	r, err := store.IsInlist(blacklist_prefix, domain)
	if err != nil {
		log.Printf("fail to test exists on key %s:%s (%s) ", blacklist_prefix, domain, err)
		// cannot reach db: by default assume domain is not in blacklist
		return false
	}
	return r

}

func (store *Store) ClearBlacklist(domain string) error {

	err := store.RemoveFromlist(whitelist_prefix, domain)
	return err
}

//
//   Whitelist
//

func (store *Store) AddWhitelist(domain string, ttl uint64) error {

	// add a domain to the whitelist
	return store.AddToList(whitelist_prefix, domain, ttl)

}

func (store *Store) IsInWhitelist(domain string) bool {
	// check domain return False if domain is in blaklist
	// True if not in whitelist or cannot read db

	r, err := store.IsInlist(whitelist_prefix, domain)
	if err != nil {
		log.Printf("fail to exists %s ", err)
		// cannot reach db: by default assume domain is whitelist
		return true
	}
	return r

}

func (store *Store) ListWhitelist() ([]string, error) {
	//
	// return list of all domain counter keys : hit:*
	//
	counter_keys, err := store.Keys(whitelist_prefix)
	return counter_keys, err
}

func (store *Store) ClearWhitelist(domain string) error {

	err := store.RemoveFromlist(whitelist_prefix, domain)
	return err

}

//
// drop counter
//
func (store *Store) IncrDropCounter(domain string, info string) error {
	// increment counter for this domain  , drop:<domain>
	// set a ttl
	ttl := drop_ttl
	key := store.dropKey(domain)
	store.Lock()
	n, err := store.Conn.Do("INCR", key)
	store.Unlock()
	if err != nil {
		log.Printf("fail to increment drop counter :%s (%s) ", domain, err)
	} else {
		// set OK: try expire
		//log.Printf("drop counter incremented for domain: %s ==> %d" , domain,n)
		if ttl != 0 {
			// set OK: try expire
			if n == 1 {
				// first assignation for the drop counter
				expire := fmt.Sprintf("set expire on drop counter %s :%d", domain, ttl)
				store.Lock()
				_, err = store.Conn.Do("EXPIRE", key, expire)
				store.Unlock()
				if err != nil {
					log.Printf("fail to set expire on %s (%s)", key, err)
				}
			}
		}
	}
	return err
}

func (store *Store) GetDropCounter(domain string) int64 {
	// get counter (int) for this  , hit:<domain>
	// return counter value or 0
	var counter int64 = 0
	key := store.dropKey(domain)
	store.Lock()
	v, err := store.Conn.Do("GET", string(key))
	store.Unlock()
	if err != nil {
		log.Printf("fail to get domain count :%s ", err)

	} else {
		//
		//i := strconv.Atoi(v.(string))
		if counter, err = strconv.ParseInt(string(v.([]uint8)), 10, 64); err == nil {
			fmt.Printf("counter =%d, type: %T\n", counter, counter)
		} else {
			fmt.Printf("cannot convert redis counter value: %s", v)
		}

	}
	return counter
}

func (store *Store) ClearDropCounter(domain string) error {
	// del counter for this domain , hit:<domain>
	// return error
	key := store.dropKey(domain)
	store.Lock()
	_, err := store.Conn.Do("DEL", key)
	store.Unlock()
	if err != nil {
		log.Printf("fail to delete domain counter :%s ", err)

	} else {
		//
		//log.Printf("counter deleted for domain: %s" , domain)
	}
	return err
}

///
///   lists ( whitelist, blacklist )
///     IsInList, addToList , getList
///

func (store *Store) AddToList(prefix string, name string, ttl uint64) error {

	// add a domain to the blacklist/whitelist
	key := store.listKeyName(prefix, name)
	store.Lock()
	_, err := store.Conn.Do("SET", key, ".")
	store.Unlock()
	if err != nil {
		log.Printf("fail to set %s:%s (%s)", prefix, name, err)
	} else {
		if ttl != 0 {
			// set OK: try expire
			expire := fmt.Sprintf("%d", ttl)
			store.Lock()
			_, err = store.Conn.Do("EXPIRE", key, expire)
			store.Unlock()
			if err != nil {
				log.Printf("fail to set %s:%s (%s)", prefix, name, err)
			}
		}
	}
	return err
}

func (store *Store) IsInlist(prefix string, name string) (bool, error) {
	// check domain is in blacklist/whitelist

	// add a domain to the blacklist
	key := store.listKeyName(prefix, name)
	store.Lock()
	exists, err := redis.Bool(store.Do("EXISTS", key))
	store.Unlock()
	//if err != nil {
	//	log.Printf("fail to perform EXISTS on domain %s:%s (%s)",prefix,name,err)
	//}
	return exists, err
}

func (store *Store) RemoveFromlist(prefix string, name string) error {

	// add a domain to the blacklist
	key := store.listKeyName(prefix, name)
	store.Lock()
	_, err := store.Conn.Do("DEL", key)
	store.Unlock()

	if err != nil {
		log.Printf("fail to remove %s:%s (%s)", prefix, name, err)
	}
	return err
}

//  set and unset rules ( for TXT , WKS , NULL ...)
//  rules/TXT ,

func (store *Store) SetRule(name, value string, ttl uint64) error {
	// set the rule with a threshold value  , rule:<name>   eg rule:TXT 3

	store.Lock()
	defer store.Unlock()

	// distinct domains counter
	// in fact domain is the etlp_p1 and info is the domain  hits:<etlp_p1>
	key := rule_prefix + ":" + name
	_, err := store.Conn.Do("SET", key, value)
	if err != nil {
		log.Printf("fail to set rule :%s (%s) ", name, err)
	} else {
		// set OK: try expire
		//log.Printf("hit counter incremented for domain: %s ==> %d" , domain,n)
		if ttl != 0 {
			// set OK: try expire
			expire := fmt.Sprintf("%d", ttl)
			_, err = store.Conn.Do("EXPIRE", key, expire)
			if err != nil {
				log.Printf("fail to set %s:%s (%s)", rule_prefix, name, err)
				return err
			}
		}
	}
	return err
}

func (store *Store) GetRule(name string) (value string, err error) {
	//
	// return value for the given rule name ( eg TXT, WKS , NULL )
	store.Lock()
	defer store.Unlock()

	//
	key := rule_prefix + ":" + name
	value, err = redis.String(store.Conn.Do("GET", key))

	return value, err
}

// keys

func (store *Store) Keys(prefix string) ([]string, error) {
	//
	// list of all domains for prefix   ( eg hit return keys hit:*)
	//
	// return error
	var keys []string

	key := prefix + ":*"
	store.Lock()
	fetch_keys, err := redis.Values(store.Conn.Do("KEYS", key))
	store.Unlock()
	if err != nil {
		log.Printf("fail to fetch keys :%s  (%s)", key, err)

	} else {
		//
		l := len(fetch_keys)
		log.Printf("fetch counter keys ok (%d)", l)
		switch reflect.TypeOf(fetch_keys).Kind() {
		case reflect.Slice:
			for _, value := range fetch_keys {
				str := string(value.([]uint8))
				// remove the prefix
				str = str[len(prefix)+1:]
				keys = append(keys, str)
				//fmt.Println(str)
			}
		}

	}
	return keys, err
}

//
//  config get /set
//

// return local config
func (store *Store) GetConfig() (jsonConfig []uint8, err error) {

	key := "config"
	store.Lock()
	_, err = store.Conn.Do("GET", key)
	store.Unlock()
	if err != nil {
		log.Printf("fail to get config (%s)", err)
	}
	return jsonConfig, err
}

func (store *Store) SetConfig(jsonConfig []uint8) (err error) {

	key := "config"
	store.Lock()
	_, err = store.Conn.Do("SET", key, jsonConfig)
	store.Unlock()
	if err != nil {
		log.Printf("fail to set config (%s)", err)
	}
	return err
}

//
//  publish query
//

func (store *Store) PublishQuery(id uint32, event map[string]interface{}) {

	//  update the prometheus metrics ( need metrics.Metric initialized )
	_ = metrics.Metric

	//metrics.Metric.RecordHitIncrement(event["et"])
	return
}

func (store *Store) PublishQuestion(id uint32, event map[string]interface{}) {

	//  update the prometheus metrics ( need metrics.Metric initialized )
	//_= metrics.Metric

	metrics.Metric.RecordHitIncrement(
		string(event["dns.question.etld_plus_one"].(string)),
		string(event["dns.question.name"].(string)))
	return
}

func (store *Store) PublishDrop(etld_p1 string) {

	//  update the prometheus metrics ( need metrics.Metric initialized )
	//_= metrics.Metric

	metrics.Metric.RecordDropIncrement(
		etld_p1,
	)
	return
}

//
// key name resolvers
//

func (store *Store) listKeyName(prefix string, element string) string {

	//compute key
	key := prefix + ":" + element
	return key

}

func (store *Store) blacklistKey(domain string) string {

	//compute key
	key := blacklist_prefix + ":" + domain
	return key

}

func (store *Store) whitelistKey(domain string) string {

	//compute key
	key := whitelist_prefix + ":" + domain
	return key

}

func (store *Store) hitKey(domain string) string {

	//compute key
	key := counter_prefix + ":" + domain
	return key

}

func (store *Store) dropKey(domain string) string {

	//compute key
	key := drop_prefix + ":" + domain
	return key

}

//
//  configuration handler
//

func (store *Store) SetParameters(dict map[string]string) error {
	//
	//  set configuration from a map of strings
	//       scan_period , scan_limit, blacklist_ttl
	var err error

	err = nil

	//store.Lock()
	//_, err = store.Conn.Do("HSET", key, expire)
	//store.Unlock()
	//if err != nil {
	//	log.Printf("fail to set %s:%s (%s)", prefix, name, err)
	//}
	return err

}

//func (store * Store) GetParameter ( name string )  (string,error) {
//	//
//	//  set configuration from a map of strings
//	//       scan_period , scan_limit, blacklist_ttl
//	//var err error
//
//	key := parameters_prefix + ":" + name
//
//	store.Lock()
//	//value , err := store.Conn.Do("HGET", key)
//	value,err := redis.Values(store.Conn.Do("HGET" ,key))
//	store.Unlock()
//	if err != nil {
//		log.Printf("fail to get parameter %s (%s)", name, err)
//		value = nil
//	}
//	return value, error
//
//}
