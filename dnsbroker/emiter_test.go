package dnsbroker_test

import(
	"testing"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"

	"github.com/davecgh/go-spew/spew"
	"github.com/AsynkronIT/protoactor-go/log"
)

var (
	name = "hive"
	url = "redis://localhost:6379/1"
)





func TestClient(t *testing.T) {
	//

	//var err error
	//var value string

	text:=  "hello there\n"
	//key := "queue:container:stdin:queue:id:1"

	hive := dnsbroker.NewHive(name,url)
	err := hive.Check(false)
	if err != nil {
		log.Error(err)
	}

	println(hive.Name + "\n")
	println(hive.Url + "\n")


	client := dnsbroker.NewRedisClient("me")

	println(client)


	client.Subscribe("me")
	//client.PSubscribe("me.*")

	// consume subscription confirmation
	client.Receive()

	client.Publish("me",text)
	client.Publish("me.info",text)


	client.Unsubscribe()
	//client.PUnsubscribe()


}


//func responder(hive actors.RedisHive){
//
//
//	responder :=actors.NewRedisClient(&hive)
//	responder.Subscribe("responder")
//	// read responder subscribe
//	response := responder.Receive()
//	spew.Dump(response)
//
//	// responder subscribe to request channels
//	responder.PSubscribe("responder.request.*")
//	// read responder subscribe
//	response = responder.Receive()
//	spew.Dump(response)
//
//
//
//	for {
//		// 2:  responder receive request
//		// read responder subscribe
//		response = responder.Receive()
//		spew.Dump(response)
//
//		// 3:  responder send response to responder.reply.1
//		time.Sleep(2 * time.Second)
//		responder.Publish("responder.reply.1", "this my response for request 1")
//		responder.Flush()
//
//	}
//
//}






func TestRequest(t *testing.T) {
	//

	text:=  "hello there\n"
	println(text)


	hive := dnsbroker.NewHive(name,url)
	hive.Check(true)

	println(hive.Name + "\n")
	println(hive.Url + "\n")



	responder := dnsbroker.NewRedisClient("me")
	responder.Subscribe("responder")
	// read responder subscribe
	//response = responder.Receive()
	//spew.Dump(response)

	// responder subscribe to request channels
	responder.Subscribe("responder.request.*")
	// read responder subscribe
	response := responder.Receive()
	spew.Dump(response)

	// create a requester for request #1

	//requester := actors.NewRedisClient(hive)
	//requester.Subscribe("requester.1")
	//// read requester subscribe
	//response = requester.Receive()
	//spew.Dump(response)


	//// 1: requester send request number 1
	//
	//// 1:a subscribe to reply:1
	//requester.Subscribe("responder.reply.1")
	//response = requester.Receive()
	//spew.Dump(response)
	//
	//// 1:b send message to response as request:1
	//requester.Publish("responder.request.1","this is my request 1")
	//requester.Flush()
	//time.Sleep(1 * time.Second)
	//
	//
	//// 1;c  scheduled a timeout message to cancel the next Receive
	//go func() {
	//	time.Sleep( 5 * time.Second)
	//	requester.Publish("responder.reply.1","TIMEOUT waiting responder.reply.1")
	//	requester.Flush()
	//}()
	//
	//
	//// 2:  responder receive request
	//// read responder subscribe
	//response = responder.Receive()
	//spew.Dump(response)
	//
	//
	//// 3:  responder send response to responder.reply.1
	//time.Sleep( 2 * time.Second)
	//responder.Publish("responder.reply.1","this my response for request 1")
	//responder.Flush()
	//
	//
	//// 4 requester wait for reply on responder.reply.1
	//response = requester.Receive()
	//spew.Dump(response)
	//
	//// 5 requester unsubscribe to responder.reply.1
	//requester.Unsubscribe("responder.reply.1")
	//response = requester.Receive()
	//spew.Dump(response)
	//
	//
	//
	//time.Sleep( 5 * time.Second)
	//
	//
	//
	////// read responder message
	////response = responder.Receive()
	////spew.Dump(response)
	////
	////// must block responder message
	////response = responder.Receive()
	////spew.Dump(response)
	//
	//
	//// requester send message on channel  responder.request:1
	//
	//
	//// responder receive message on channel responder.request:1
	//
	//
	//
	//// responder send response to responder.rpc:1 to responder.reply:1



}



