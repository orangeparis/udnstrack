package dnsbroker_test

import (
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var HIVE_NAME = "hive"
var REDIS_URL = "redis://localhost:6379/0"

//var REDIS_URL = "redis://192.168.99.100:6379/0"

func newHive() *dnsbroker.Hive {

	name := HIVE_NAME
	url := REDIS_URL

	dnsbroker.ResetHive()

	h := dnsbroker.NewHive(name, url)
	hive := dnsbroker.GetHive()

	println(hive == h)

	return hive

}

func TestBlacklist(t *testing.T) {

	var err error

	domain := "orangedns.club."

	// reset hive
	hive := newHive()

	//// get a store instance
	store := dnsbroker.NewStore()

	err = store.ClearBlacklist(domain)
	if err != nil {
		println("failed", err)
	}

	// check domain is not in blacklist
	ok := store.IsInBlacklist(domain)
	println("ok: ", ok)
	assert.Equal(t, ok, false)

	// add domain in blacklist for 5 seconds
	store.AddBlacklist(domain, 5)

	// check domain is in black list
	ok = store.IsInBlacklist(domain)
	println("ok: ", ok)
	assert.Equal(t, ok, true)

	// wait 7 seconds so domain is not in blacklist anymore
	time.Sleep(7 * time.Second)

	// check domain is not in blacklist anymore
	ok = store.IsInBlacklist(domain)
	println("ok: ", ok)
	assert.Equal(t, ok, false)

	hive.Close()
}

func TestWhitelist(t *testing.T) {

	var ok bool
	var err error

	domain := "google.com"

	// reset hive
	hive := newHive()

	// get a store instance
	store := dnsbroker.NewStore()

	// clear whitelist
	err = store.ClearWhitelist(domain)
	assert.Equal(t, err, nil)

	// check domain is not in blacklist
	ok = store.IsInWhitelist(domain)
	println("ok: ", ok)
	assert.Equal(t, ok, false)

	// add domain in blacklist for 5 seconds
	store.AddWhitelist(domain, 5)

	// check domain is in white list
	ok = store.IsInWhitelist(domain)
	println("ok: ", ok)
	assert.Equal(t, ok, true)

	// clear whitelist
	err = store.ClearWhitelist(domain)
	assert.Equal(t, err, nil)

	// check domain is not in blacklist anymore
	ok = store.IsInWhitelist(domain)
	println("ok: ", ok)
	assert.Equal(t, ok, false)

	hive.Close()
}

func TestDomainCounter(t *testing.T) {

	var err error

	// reset hive
	hive := newHive()

	// get a store instance
	store := dnsbroker.NewStore()

	// add a count to a domain
	domain := "orangedns.club."

	// clear domain counter
	err = store.ClearDomainCounter(domain)
	println("ok: ", err)
	assert.Equal(t, err, nil)

	err = store.IncrDomainCounter(domain, "dummy.com")
	println("ok: ", err)
	assert.Equal(t, err, nil)

	// read counter for domain
	counter := store.GetDomainCounter(domain)

	assert.Equal(t, counter, uint64(1))

	hive.Close()

}

func TestListDomainCounter(t *testing.T) {

	// reset hive
	hive := newHive()

	// get a store instance
	store := dnsbroker.NewStore()

	// fetch domain counters
	counters, err := store.ListDomainCounter()
	println("fetch domain counters %s,%s", counters, err)

	//switch reflect.TypeOf(counters).Kind() {
	//case reflect.Slice:
	//	for _, value := range counters {
	//		str := string(value.([]uint8))
	//		fmt.Println(str)
	//	}
	//}

	hive.Close()

}

func TestSetRules(t *testing.T) {

	var err error

	// reset hive
	hive := newHive()

	// get a store instance
	store := dnsbroker.NewStore()

	// clear whitelist
	err = store.SetRule("TXT", "0", 10)
	assert.Equal(t, err, nil)

	// check domain is not in blacklist
	value, err := store.GetRule("TXT")
	assert.Equal(t, value, "1")

	hive.Close()
}
