package dnsbroker

import (
	"encoding/json"
	"github.com/BurntSushi/toml"
	"log"
)

var (
	configFilename string = "dnstrack.toml"
	dnstrackConfig *DnstrackConfig
)

type DnstrackConfig struct {
	Version string

	// dry mode: if true dnsguard will not drop packets (always accept packets )
	// simulation_mode in backend config
	DryMode bool

	// prefix to store blacklisted domains  Eg  blacklist:piratedns.io.
	BlacklistPrefix string `toml:"blacklist_prefix"`

	// time to live for a blacklisted domain
	BlacklistTTL uint64 `toml:"blacklist_ttl"`

	// time to live for a blacklisted domain
	WhitelistTTL uint64 `toml:"whitelist_ttl"`

	// time to live for a droplist domain
	DroplistTTL uint64 `toml:"droplist_ttl"`

	// period for scanning counters ( seconds )
	ScanPeriod uint64 `toml:"scan_period"`
	// limit of unique hostname for a domain to detect a tunnel over the scan_period
	ScanLimit uint64 `toml:"scan_limit"`

	// a list of domains
	WhiteList []string

	Blacklist []string

	Backend BackendConfig

	Redis    RedisConfig
	Influxdb InfluxDbConfig

	Dnswatch DnswatchConfig

	DnsGuard DnsguardConfig
}

// get a json representation
func (c *DnstrackConfig) Marshal() (jsonConfig []uint8, err error) {
	return json.Marshal(c)
}

type PropertiesConfig struct {
	Properties map[string]string
}

type BackendConfig struct {

	// backend admin
	Master      string // host of backend  ( eg 192.168.99.100:3333
	MasterToken string // backend basic authentification Authorization: Basic XXXXXXXXXXXXX
	MasterUrl   string // address to get config  to get /api/agents/<proId>/config

	// prometheus pushgateway
	PushGateway   string // host of prometheus push gateway eg ( 192.168.99.100:9091 )
	PrometheusUrl string // adrress to send prometheus metrics

	// identification key to request config  /api/v1/agents/<key>/config  default: demo
	ProbeId string

	// prometheus job to export metrics ( default = dnsguard )
	Job string

	//  polling time in seconds to fetch config updates
	// get_config_frequency in backend config
	PollConfigFrequency int
}

type RedisConfig struct {
	Url string
}

type InfluxDbConfig struct {
	Url      string
	Db       string
	Username string
	Password string

	FlushDelay uint `toml:"flush_delay"`
}

type DnswatchConfig struct {
	// time interval in s between 2 scan of the database to find tunnels
	ScanInterval uint `toml:"scan_interval"`
	// duration in s of the scan to find tunnels
	ScanPeriod uint `toml:"scan_period"`
	// limit of unique hostname for a domain to detect a tunnel over the scan_period
	ScanLimit uint `toml:"scan_limit"`
}

type DnsguardConfig struct {
	Name string

	QuestionChannel string `toml:"question_channel"`
	DropChannel     string `toml:"drop_channel"`
	QueryChannel    string `toml:"query_channel"`

	ActiveChannels []string `toml:"active_channels"`
}

//// create a new Config
//func NewConfig() * DnstrackConfig {
//
//
//	dnstrackOnce.Do(func() {
//		// create a new config singleton
//		if _, err := toml.DecodeFile(configFilename, &dnstrackConfig); err != nil {
//			log.Fatal(err)
//		}
//
//	})
//
//	return dnstrackConfig
//}

// create a new Config
func NewConfig(configFileName string) *DnstrackConfig {

	if dnstrackConfig != nil {
		log.Fatal("configuration already instanciated")

	}

	if _, err := toml.DecodeFile(configFilename, &dnstrackConfig); err != nil {
		log.Fatal(err)
	}

	return dnstrackConfig
}

func GetConfig() *DnstrackConfig {

	if dnstrackConfig == nil {
		log.Fatal("No Config has been instanciated with NewConfig(filename)")
	}

	// create a new config
	var config DnstrackConfig
	// copy the original config
	config = *dnstrackConfig

	// return the copy
	return &config
	//return dnstrackConfig
}
