
an internal http server to supervise the udnsguard


* view redis status for
 * blacklist   (/blacklist)
 * hits        (/hit)
 * drop        (/drop)
 
* get config (/config)
 
* serve prometheus metrics


counters:

    hit:
        etld+1
        
    drop:
        etld+1
        
    blacklist: 
        etld+1 domain
        