package server

import (
	"log"
	"net/http"
	//"os"
	"fmt"
	//"time"
	"encoding/json"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"bitbucket.org/orangeparis/udnstrack/metrics"
)

var (

	// global serverStore
	serverStore *dnsbroker.Store
)

//func init() {
//
//
//	// init Prometheus Metric
//	//hostname, _ := os.Hostname()
//	metrics.InitMetric()
//	//metrics.Metric = &metrics.DnsguardMetrics{ Name: hostname}
//	//metrics.Metric.InitMetrics()
//
//}

//func observer() {
//	// a task to periodically record goroutine
//	for {
//		metrics.Metric.RecordGoRoutine()
//		time.Sleep(1 * time.Second)
//	}
//}

func customHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "welcome to dnstrack home page\n")
}

// handle    /hit/  and /hit/mysite.com
func HitHandler(w http.ResponseWriter, r *http.Request) {

	domain := r.URL.Path[len("/hit/"):]

	if len(domain) == 0 {
		// request for all hits    hit:*
		l, _ := serverStore.Keys("hits")
		json.NewEncoder(w).Encode(l)
	} else {
		// request for only one site  hit:mysite.com
		site := fmt.Sprintf("hits:%s.", domain)
		hits := serverStore.GetDomainCounter(site)

		resp := make(map[string]uint64)
		resp[domain] = hits
		json.NewEncoder(w).Encode(resp)

	}
	return
}

func DropHandler(w http.ResponseWriter, r *http.Request) {

	domain := r.URL.Path[len("/drop/"):]

	if len(domain) == 0 {
		// request for all hits    hit:*
		l, _ := serverStore.Keys("drop")
		json.NewEncoder(w).Encode(l)
	} else {
		// request for only one site  hit:mysite.com
		site := fmt.Sprintf("drop:%s.", domain)
		hits, err := serverStore.Conn.Do("Get", site)

		resp := make(map[string]string)
		if err == nil && hits != nil {
			resp[domain] = string(hits.([]uint8))
			json.NewEncoder(w).Encode(resp)
		} else {
			json.NewEncoder(w).Encode(nil)
		}
	}
	return
}

func BlacklistHandler(w http.ResponseWriter, r *http.Request) {

	domain := r.URL.Path[len("/blacklist/"):]

	if len(domain) == 0 {
		// request for all hits    hit:*
		l, _ := serverStore.Keys("blacklist")
		json.NewEncoder(w).Encode(l)
	} else {
		// request for only one site  hit:mysite.com
		site := fmt.Sprintf("blacklist:%s.", domain)
		hits, err := serverStore.Conn.Do("Get", site)

		resp := make(map[string]string)
		if err == nil && hits != nil {
			resp[domain] = string(hits.([]uint8))
			json.NewEncoder(w).Encode(resp)
		} else {
			json.NewEncoder(w).Encode(nil)
		}
	}
	return
}

// read config file and set config to redis
func SetConfig() (err error) {

	conf := dnsbroker.GetConfig()
	json_conf, err := conf.Marshal()
	if err != nil {
		log.Printf("cannot jsonify config: %s", err)
		return err
	}
	err = serverStore.SetConfig(json_conf)

	return err
}

// handle /simulate//hit/mysite.com
func simulateHitHandler(w http.ResponseWriter, r *http.Request) {

	domain := r.URL.Path[len("simulate/hit/"):]

	metrics.Metric.RecordHitIncrement(domain, domain)
}

func App() (router *http.ServeMux) {

	//  initialize serverStore
	serverStore = dnsbroker.NewStore()

	router = http.NewServeMux()

	// handle root
	router.HandleFunc("/", homePage)

	// handle prometheus metrics
	router.Handle("/metrics", promhttp.Handler())

	// handle /hit/
	router.HandleFunc("/hit/", HitHandler)

	// handle /drop/
	router.HandleFunc("/drop/", DropHandler)

	// handle /blacklist/
	router.HandleFunc("/blacklist/", BlacklistHandler)

	// handle /custom
	router.HandleFunc("/custom/", customHandler)

	// handle /custom
	router.HandleFunc("/simulate/hit/", simulateHitHandler)

	// simulate backend api  ( /api/v1/agents/<probeId>/config
	router.HandleFunc("/api/", MasterConfig)

	return router
}

func Run() {

	// create the configuration singleton   ( task will ask dnsbroker.GetConfig() )
	// conf := dnsbroker.NewConfig("./dnstrak.toml")
	// create the redis hive singleton
	// dnsbroker.NewHive( "server",conf.Redis.Url)

	router := App()

	// store redis config
	SetConfig()

	log.Fatal(http.ListenAndServe("0.0.0.0:3333", router))

}
