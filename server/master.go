package server

import (

	//"log"
	"net/http"
	//"os"
	"fmt"
	//"time"
	//"encoding/json"
	//"github.com/prometheus/client_golang/prometheus/promhttp"
	//"bitbucket.org/orangeparis/udnstrack/metrics"
	//"bitbucket.org/orangeparis/udnstrack/dnsbroker"
)

/*

	simulates ${backend.MasterUrl}/api/v1/agents/<probeId>/config

*/

// a sample basic backend config
var sample_backend_config = `
{
  "config":
  {
    "get_config_frequency": 300,
    "simulation_mode": false,
    "push_detected_domains_frequency": 60
  },
  "blacklist": [
    "freedom.net",
    "12345azer.de"
  ],
  "whitelist": [
    "google.com",
    "orange.fr"
  ],
  "rules": [
    {
      "field_label": "TXT",
      "rule_label": "txt_rule",
      "max_length": 0
    },
    {
      "field_label": "NULL",
      "rule_label": "null_rule",
      "max_length": 10
    },
    {
      "field_label": "WKS",
      "rule_label": "wks_rule",
      "max_length": 0
    }
  ]
}
`

// the config return by default at https://agentconnectorint.kmt.orange.com/api/v1/agents/DEMO/config
var dummy_backend_config = `
{
  "rules": [
    {
      "field_label": "TXT",
      "rule_label": "txt_rule",
      "params": null,
      "rule_id": null
    },
    {
      "field_label": "NULL",
      "rule_label": "null_rule",
      "params": null,
      "rule_id": null
    }
  ],
  "blacklist": [
    "freedom.net",
    "12345azer.de"
  ],
  "whitelist": [
    "google.com",
    "orange.fr"
  ],
  "config": {
    "get_config_frequency": null,
    "domains_timeout": null,
    "push_detected_domains_frequency": null
  }
}
`

// handle  /api/*
//   GET /api/v1/agents/<probeId>/config/   => return a sample backendconfig json file
func MasterConfig(w http.ResponseWriter, r *http.Request) {
	/*
		return the json config stored in redis
	*/

	//config,_ := serverStore.GetConfig()
	//config := "a config"
	//fmt.Fprintf(w, string(config))

	//js, err := json.Marshal(config)
	//if err != nil {
	//	http.Error(w, err.Error(), http.StatusInternalServerError)
	//	return
	//}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(sample_backend_config))
	//json.NewEncoder(w).Encode(config)

	return
}
