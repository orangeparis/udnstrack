package server

import (
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"bitbucket.org/orangeparis/udnstrack/metrics"
	"testing"
)

func TestServer(t *testing.T) {

	// init metrics
	metrics.InitMetric()

	//// initialize hive
	//hive := dnsbroker.NewHive("store","redis://127.0.0.1:6379/0")
	//_= hive
	//
	//
	//// test redis connection
	//hive = dnsbroker.GetHive()
	//con := hive.NewConn()
	//defer con.Close()
	//
	//r,err := con.Do("PING")
	//_=r
	//_=err

	// create the configuration singleton   ( task will ask dnsbroker.GetConfig() )
	conf := dnsbroker.NewConfig("./dnstrak.toml")
	// create the redis hive singleton
	dnsbroker.NewHive("server", conf.Redis.Url)

	// launch an http server at localhost:3333
	// GET localhost:3333/metrics
	Run()

}
