package tests

/*

	a configuration to test filter + watcher / server

	the source is a pcap file so it can be run on a mac ( no netfilter on mac )



 */



import (
	"log"
	"testing"
	"bitbucket.org/orangeparis/udnstrack/metrics"
	"bitbucket.org/orangeparis/udnstrack/udnswatch"
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"bitbucket.org/orangeparis/udnstrack/server"
	"bitbucket.org/orangeparis/udnstrack/filePlayer"

	"sync"
	//"os"
	//"time"
	//"fmt"
	"fmt"
)

var (
	name = "hive"

	url = "redis://redis:6379/0"

	wg sync.WaitGroup

)



type Object struct {
	//data
}


// a coroutine for a guardian
func GuardianTask(wg *sync.WaitGroup) {
	//
	log.Println("starting fileplayer guardian")
	guardian,err := filePlayer.NewPlayer(pcapFile)
	if err != nil {
		log.Printf("failed to start file player: %s\n", err.Error())
	}
	defer guardian.Close()
	guardian.Start()
	wg.Done()
	log.Println("fileplayer guardian closed")
	return
}



// a coroutine for a watcher
func WatcherTask(wg *sync.WaitGroup) {
	//
	log.Println("starting dnswatcher")
	watcher := udnswatch.NewWatcher()
	defer watcher.Close()
	watcher.Run()
	wg.Done()
	log.Println("dnswatcher closed")
	return
}

// a coroutine for a httpserver
func ServerTask(wg *sync.WaitGroup) {
	//
	log.Println("starting http server")
	server.Run()
	wg.Done()
	log.Println("http server closed")
	return
}




func TestTrackerWithPcapFile(t *testing.T) {



	// setup the context

	// create the configuration singleton   ( task will ask dnsbroker.GetConfig() )
	conf := dnsbroker.NewConfig("./dnstrak.toml")


	// create the hive singleton for redis Clients  ( task will ask dnsbroker.
	dnsbroker.NewHive(name,conf.Redis.Url)


	// setup the prometheus metrics recorder
	metrics.InitMetric()


	// get the context
	//cnf := dnsbroker.GetConfig()

	// set the wait group
	wg.Add(3)

	go WatcherTask(&wg)
	go ServerTask(&wg)


	// start the coroutines
	go GuardianTask(&wg)





	// wait for task to end
	wg.Wait()

	fmt.Println("Group done")


}

