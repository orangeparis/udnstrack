package tests


import (

	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
)



var HIVE_NAME = "hive"
var REDIS_URL = "redis://localhost:6379/0"
//var REDIS_URL = "redis://192.168.99.100:6379/0"


func newHive() * dnsbroker.Hive {

	name := HIVE_NAME
	url := REDIS_URL

	dnsbroker.ResetHive()

	h := dnsbroker.NewHive(name,url)
	hive := dnsbroker.GetHive()

	println(hive==h)

	return hive

}
