
package tests



// Use tcpdump to create a test file
// tcpdump -w test.pcap


import (
	"testing"
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"log"
	"time"

)

var (
	pcapFile string = "/Users/cocoon/tmp/app-traffic.pcap"
	pcapDevice string = "en0"
	handle   *pcap.Handle
	err      error
)



func TestPrintPcapFromeFile( t *testing.T) {

	// open a pcap file and print each packet to screen


	// Open file instead of device
	handle, err = pcap.OpenOffline(pcapFile)
	if err != nil { log.Fatal(err) }
	defer handle.Close()

	// Loop through packets in file
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	for packet := range packetSource.Packets() {
		fmt.Println(packet)
	}
	return

}


func TestPrintPcapFromDevice( t *testing.T) {


	// open a device interface (eth0) and print each packet to screen

	// Open device
	snapshot_len := int32(1024)
	promiscuous := false
	var timeout time.Duration = 30 * time.Second

	handle, err = pcap.OpenLive(pcapDevice, snapshot_len, promiscuous, timeout)
	if err != nil {log.Fatal(err) }
	defer handle.Close()

	// Use the handle as a packet source to process all packets
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	for packet := range packetSource.Packets() {
		// Process packet here
		fmt.Println(packet)
	}
}





