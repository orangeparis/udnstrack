package filters

import (
	"log"
	//"time"
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"golang.org/x/net/publicsuffix"
	//"strconv"
	"strings"
)

type FilterEngine interface {
	Filter(rawPacket *gopacket.Packet) (event map[string]interface{}, err error)
	Check_blacklist_rules(dns layers.DNS) (verdict string)
}

type Filter struct {
	publisher *dnsbroker.Store
}

func NewFilter(publisher *dnsbroker.Store) (filter *Filter) {

	log.Printf("create new Filter\n")

	filter = &Filter{
		publisher: publisher,
	}

	return filter
}

func (f *Filter) Filter(packet gopacket.Packet, packetId uint32) (event map[string]interface{}, err error) {
	//
	//
	//
	event = make(map[string]interface{})
	event["_verdict"] = "PASS"
	event["udp_size"] = 0

	var (
		eth  layers.Ethernet
		ip4  layers.IPv4
		ip6  layers.IPv6
		tcp  layers.TCP
		udp  layers.UDP
		dns  layers.DNS
		body gopacket.Payload
	)

	parser := gopacket.NewDecodingLayerParser(
		layers.LayerTypeEthernet,
		&eth,
		&ip4,
		&ip6,
		&tcp,
		&udp,
		&dns,
		&body)

	var foundLayerTypes []gopacket.LayerType
	err = parser.DecodeLayers(packet.Data(), &foundLayerTypes)
	if err != nil {
		log.Printf("[DecodeLayers error   ] %v %v", err, foundLayerTypes)
		return event, err
	}

	isDns := false

	for _, layer := range foundLayerTypes {
		switch layer {

		case layers.LayerTypeIPv4:
			event["SrcIP"] = ip4.SrcIP.String()
			event["DstIP"] = ip4.DstIP.String()
			event["dummy"] = ip4.Length
		case layers.LayerTypeIPv6:
			event["SrcIP"] = ip6.SrcIP.String()
			event["DstIP"] = ip6.DstIP.String()
		case layers.LayerTypeUDP:
			event["udp_size"] = udp.Length
		case layers.LayerTypeDNS:
			isDns = true
		}
	}

	if isDns == false {
		// not a dns request
		log.Printf("Not a dns Packet")
		return event, nil
	}

	//
	// we got a dns packet
	//
	//log.Printf("Got a Dns packet")
	event["type"] = "dns"
	event["dns.id"] = dns.ID
	event["dns.op_code"] = dns.OpCode
	event["dns.response_code"] = dns.ResponseCode
	event["questions_count"] = dns.QDCount
	event["answers_count"] = dns.ANCount
	// feed question
	event["dns.question.name"] = ""
	for _, dnsQuestion := range dns.Questions {
		event["dns.question.name"] = string(dnsQuestion.Name)
		event["dns.question.dnstype"] = dnsQuestion.Type // A CNAME TXT ...
		// only take the first question
		break
	}

	//if event["udp_size"] > 0 {
	//	event["dns.opt.udp_sizeudp_size"] = event["udp_size"]
	//}

	//event["dns.Questions"]= dns.Questions
	//event["dns.Answers"]= dns.Answers

	reply := dns.QR
	if reply == false {

		// this is a simple query (no answers)
		//log.Printf("got a dns question  for %s ,(%d) \n", event["dns.question.name"],dns.ID,)

		//  check if domain is blacklisted
		verdict := "PASS"

		for {

			if verdict != "DROP" {

				verdict = f.Check_blacklist_rules(dns)
				if verdict == "DROP" {
					event["_verdict"] = verdict
					event["type"] = "dns_dropped"
					event["drop.reason"] = "blacklisted_domain"
					//guardian.publisher.PublishDropped(rawPacket.ID,event,dns)
					log.Print("DROP Packet : domain is in blacklist")
					break
				}
			}
			//
			// check packet with local rules
			//
			if verdict != "DROP" {

				verdict = f.Check_local_rules(event, dns)
				if verdict == "DROP" {
					event["_verdict"] = verdict
					event["type"] = "dns_dropped"
					//event["drop.reason"] = "break_rules"
					//guardian.publisher.PublishDropped(rawPacket.ID,event,dns)
					log.Print("DROP Packet according to local rules")
					break
				}
			}
			break
		}

		// publish all questions
		for _, dnsQuestion := range dns.Questions {
			name := string(dnsQuestion.Name)
			event["dns.question.name"] = name
			//event["type"] = "dns_question"
			//event["dns.question.type"] = dnsQuestion.Type
			//event["dns.question.class"] = dnsQuestion.Class
			eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(name, "."))
			if err == nil {
				event["dns.question.etld_plus_one"] = eTLDPlusOne + "."
			} else {
				event["dns.question.etld_plus_one"] = "unknown."
			}
			f.publisher.PublishQuestion(packetId, event)
		}
		return event, nil

	} else {
		// this is a reply (question+reply)
		//log.Printf("got a dns reply  for %s ,(%d) \n", event["dns.question.name"],dns.ID,)
		// publish it
		//event["type"] = "dns"
		//guardian.publisher.PublishQuery(rawPacket.ID,event)

	}

	return event, err
}

func (f *Filter) Check_blacklist_rules(dns layers.DNS) (verdict string) {

	verdict = "PASS"
	// check dns event with local rules
	for _, dnsQuestion := range dns.Questions {

		name := string(dnsQuestion.Name)
		eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(name, "."))
		if err == nil {
			eTLDPlusOne = eTLDPlusOne + "."
		} else {
			eTLDPlusOne = "unknown" + "."
		}

		// increment domain counter
		domain := string(dnsQuestion.Name)
		r := f.publisher.IncrDomainCounter(eTLDPlusOne, domain)
		if r != nil {
			log.Printf("fail to increment domain counter")
		}

		blacklisted := f.publisher.IsInBlacklist(eTLDPlusOne)
		if blacklisted == true {
			verdict = "DROP"
			// increment drop counter
			f.publisher.IncrDropCounter(eTLDPlusOne, "")
			// publish drop for supervision
			f.publisher.PublishDrop(eTLDPlusOne)
			break
		}

		//if verdict == "DROP" {
		//	break
		//}

	}
	return verdict
}

func (f *Filter) Check_local_rules(event map[string]interface{}, dns layers.DNS) (verdict string) {

	// check for local rules
	// if dns_type in [ TXT , WKS , NULL )
	verdict = "PASS"
	dns_type := event["dns.question.dnstype"].(layers.DNSType).String()

	// find rule in redis
	rule, err := f.publisher.GetRule(dns_type)
	if err == nil {
		_ = rule
		//size, err := strconv.Atoi(rule)
		//if err == nil {
		//// rule exists
		//if int(size) >= 0 {
		// we got a rule for this dns type
		event["_verdict"] = "DROP"
		event["type"] = "dns_dropped"
		event["drop.reason"] = "rule " + dns_type
		verdict = "DROP"
		log.Printf("drop packet for domain %s ,  with type= %s\n", event["dns.question.name"], dns_type)
		return verdict
		//}
		//}
	}
	return verdict
}
