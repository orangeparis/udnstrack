# Prometheus push gateway


The push gateway receives metrics from the probe (via http) 

and then is scrapped by prometheus.

We need this because the probe can be scraped for security reason ( only open port is 53)


## see
 
    https://github.com/prometheus/pushgateway#about-the-job-and-instance-labels
    
    