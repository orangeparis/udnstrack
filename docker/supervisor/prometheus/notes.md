
docker run -d -p 9090:9090 
    -v ~/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus 
    -config.file=/etc/prometheus/prometheus.yml 
    -storage.local.path=/prometheus 
    -storage.local.memory-chunks=10000