#!/usr/bin/env ash
set -ex

trap cleanup INT TERM

cleanup() {
    iptables -D OUTPUT --sport dns  -j NFQUEUE --queue-num 0
    iptables -D INPUT --dport dns -j NFQUEUE --queue-num 0
    exit
}

iptables -A INPUT --dport dns -j NFQUEUE --queue-num 0
iptables -A OUTPUT --sport dns -j NFQUEUE --queue-num 0

go run main.go