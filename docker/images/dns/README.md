dns unbound server
==================


from an article at 

https://pmig.at/2016/01/16/host-your-own-7mb-dns-server-with-docker-alpine-linux-and-unbound/



build image (build.sh)
================
    docker build -t cocoon/dns .


use (run.sh)
============

start dns server who listen on port 53

    docker run --privileged -p53:53/udp --name dns cocoon/dns
    


test (test.sh)
==============

test with dig on mac osx and boot2docker

    dig @192.168.99.100 -p 53 orange.com
    