package devicePlayer


import (
	//"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"log"
	"time"
	"os"
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"bitbucket.org/orangeparis/udnstrack/filters"
)

var (
	//device       string = "en0"
	//snapshot_len int32  = 1024
	//promiscuous  bool   = false
	//err          error
	//timeout      time.Duration = 30 * time.Second
	//handle       *pcap.Handle
)


// a redis pubsub publisher
type DevicePlayer struct {

	publisher * dnsbroker.Store
	filter * filters.Filter
	device string   // interface device eg eth0
	snapshot_len int32  // len eg 1024
	promiscuous  bool   // default  false
	timeout      time.Duration   // eg 30 * time.Second
	handle       *pcap.Handle
	counter int
}


func NewDevicePlayer( device string ) (player * DevicePlayer,err error ) {

	//var handle  *pcap.Handle

	log.Printf("create new Device player for interface \n")

	snapshot_len := int32(1024)
	promiscuous := false
	timeout := time.Duration(30 * time.Second)

	handle, err := pcap.OpenLive(device, snapshot_len, promiscuous, timeout)
	if err != nil { return player ,err}

	//
	//hive := dnsbroker.NewHive(name,url)
	publisher := dnsbroker.NewStore()
	filter := filters.NewFilter(publisher)

	//
	player = &DevicePlayer{
		filter: filter,
		publisher: publisher,
		device: device,
		snapshot_len: snapshot_len,
		timeout: timeout,
		handle: handle,
	}

	return player , err
}


func ( player  * DevicePlayer) Close() {
	//
	player.publisher.Close()
	player.handle.Close()
	log.Printf("Guardian closed: %d packets processed\n", player.counter)
	os.Exit(-1)

}

func (player * DevicePlayer) Start() {

	player.HandleDns()

}

func (player * DevicePlayer) HandleDns() {


	log.Printf("Handle Dns")
	// main loop

	log.Printf("entering main HandleDns loop\n")

	var packetId uint32


	// Loop through packets in file
	packetSource := gopacket.NewPacketSource( player.handle, player.handle.LinkType())

	packetId = 0 // (p.id )
	for p := range packetSource.Packets() {

		//spew.Dump(p)
		player.HandlePacket( p, packetId)
		player.counter++
		packetId ++
		// tempo to slow down
		time.Sleep(1 * time.Millisecond)
	}

	log.Printf("no more packets from source \n")

}


func (player * DevicePlayer) HandlePacket( packet gopacket.Packet , id uint32  ) {


	log.Printf("==================== PKT [%03d] \n", id)
	// fmt.Printf("------------------------------------------------------------------------\n id: %d\n", payload.Id)
	// fmt.Println(hex.Dump(payload.Data))
	// Decode a packet

	event, err := player.filter.Filter(packet,id)
	//println("dissector result:",err,"\n")
	if err != nil {
		println("filter error result:", err, "\n")
	} else {
		// success we have a dns event
		//spew.Dump(event)
	}

	if event["_verdict"] == "DROP" {
		log.Printf("==================== DROP PACKET: [%03d]", id)
	} else {
		log.Printf("===================== ACCEPT PACKET: [%03d]", id)

	}
	player.counter++
	// tempo to slow down
	time.Sleep(1 * time.Millisecond)
}


