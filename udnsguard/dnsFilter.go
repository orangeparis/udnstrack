/*


	event["type"]
		dns : a dns query ( question + reply )

		dns_q:  a dns question ( no-reply )

		dns_dropped  a dns question that has been dropped ( reason , local_rules or blacklist )



 */



package dnsguard

import (
	"log"
	//"os"
	//"os/signal"

	"github.com/kung-foo/freki/netfilter"
	//"github.com/davecgh/go-spew/spew"
	//"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"

	"golang.org/x/net/publicsuffix"
	"strings"

	//"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	"fmt"
)



//// a dummy ethernet header ( because netfilter squeeze it)
//var ethHdr = []byte{
//	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//	0x08, 0x00,
//}



func (guardian * Guardian) Filter(rawPacket *netfilter.RawPacket ) (map[string]interface{},error) {

	event := make(map[string]interface{})
	event["_verdict"] = "PASS"
	event["udp_size"] = 0



	packet := GopacketFromNetfilterRawPacket(rawPacket)

	// add a dummy ethernet header because netfilter rawpacket does not have it
	// see: https://github.com/kung-foo/freki/blob/master/freki.go
	//buffer := append(ethHdr, rawPacket.Data...)
	//packet := gopacket.NewPacket(
	//	buffer,
	//	layers.LayerTypeIPv4,
	//	gopacket.DecodeOptions{Lazy: false, NoCopy: true},
	//)

	var (
		eth  layers.Ethernet
		ip4   layers.IPv4
		ip6 layers.IPv6
		tcp  layers.TCP
		udp  layers.UDP
		dns  layers.DNS
		body gopacket.Payload
	)

	parser := gopacket.NewDecodingLayerParser(
		layers.LayerTypeEthernet,
		&eth,
		&ip4,
		&ip6,
		&tcp,
		&udp,
		&dns,
		&body)

	var foundLayerTypes []gopacket.LayerType
	err := parser.DecodeLayers(packet.Data(), &foundLayerTypes)
	if err != nil {
		msg := fmt.Sprintf("[DecodeLayers error   ] %v %v", err, foundLayerTypes)
		log.Println(msg)
		return event, err
	}

	isDns := false

	for _, layer := range foundLayerTypes {
		switch layer {

		case layers.LayerTypeIPv4:
			event["SrcIP"] = ip4.SrcIP.String()
			event["DstIP"] = ip4.DstIP.String()
			event["dummy"] = ip4.Length
		case layers.LayerTypeIPv6:
			event["SrcIP"] = ip6.SrcIP.String()
			event["DstIP"] = ip6.DstIP.String()
		case layers.LayerTypeUDP:
			event["udp_size"] = udp.Length
		case layers.LayerTypeDNS:
			isDns = true
		}
	}

	if isDns == false {
		// not a dns request
		log.Printf("Not a dns Packet")
		return event, nil
	}

	//
	// we got a dns packet
	//
	log.Printf("Got a Dns packet")
	event["type"] = "dns"
	event["dns.id"] = dns.ID
	event["dns.op_code"] = dns.OpCode
	event["dns.response_code"] = dns.ResponseCode
	event["questions_count"] = dns.QDCount
	event["answers_count"] = dns.ANCount
	// feed question
	event["dns.question.name"] = ""
	for _, dnsQuestion := range dns.Questions {
		event["dns.question.name"] = string(dnsQuestion.Name)
		event["dns.question.dnstype"] = dnsQuestion.Type   // A CNAME TXT ...
		// only take the first question
		break
	}

	//if event["udp_size"] > 0 {
	//	event["dns.opt.udp_sizeudp_size"] = event["udp_size"]
	//}

	//event["dns.Questions"]= dns.Questions
	//event["dns.Answers"]= dns.Answers


	reply := dns.QR
	if reply == false {

		// this is a simple query (no answers)
		log.Printf("got a dns question  for %s ,(%d) \n", event["dns.question.name"],dns.ID,)


		//  check if domain is blacklisted

		event["_verdict"] = guardian.Check_blacklist_rules(dns)
		if event["_verdict"] == "DROP" {
			event["type"] = "dns_dropped"
			event["drop.reason"] = "blacklisted_domain"
			//guardian.publisher.PublishDropped(rawPacket.ID,event,dns)
			log.Print("DROP Packet : domain is in blacklist")
			return event,nil
		}

		//
		// check packet with local rules
		//

		//event["_verdict"] = Check_local_rules(dns)
		//if event["_verdict"] == "DROP" {
		//	event["type"] = "dns_dropped"
		//	event["drop.reason"] = "break_rules"
		//	guardian.publisher.PublishDropped(rawPacket.ID,event,dns)
		//	log.Print("DROP Packet according to local rules")
		//	return event,nil
		//}

		// publish all questions
		for _, dnsQuestion := range dns.Questions {
			name := string(dnsQuestion.Name)
			event["dns.question.name"] = name
			//event["type"] = "dns_question"
			//event["dns.question.type"] = dnsQuestion.Type
			//event["dns.question.class"] = dnsQuestion.Class
			eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(name, "."))
			if err == nil {
				event["dns.question.etld_plus_one"] = eTLDPlusOne + "."
			} else {
				event["dns.question.etld_plus_one"] = "unknown."
			}
			guardian.publisher.PublishQuestion(rawPacket.ID,event)
		}
		return event,nil

	} else {
		// this is a reply (question+reply)
		log.Printf("got a dns reply  for %s ,(%d) \n", event["dns.question.name"],dns.ID,)
		// publish it
		//event["type"] = "dns"
		//guardian.publisher.PublishQuery(rawPacket.ID,event)

	}

	return event, err
}



func (guardian * Guardian) Check_blacklist_rules(dns layers.DNS) string {


	//func Check_blacklist_rules(dns layers.DNS) string {

	verdict:= "PASS"
	// check dns event with local rules
	for _, dnsQuestion := range dns.Questions {

		name := string(dnsQuestion.Name)
		eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(name, "."))
		if err == nil {
			eTLDPlusOne = eTLDPlusOne + "."
		} else {
			eTLDPlusOne = "unknown" + "."
		}


		// increment domain counter
		domain := string(dnsQuestion.Name)
		r := guardian.publisher.IncrDomainCounter(eTLDPlusOne,domain)
		if r != nil {
			log.Printf("fail to increment domain counter")
		}


		blacklisted := guardian.publisher.IsInBlacklist(eTLDPlusOne)
		if blacklisted == true {
			verdict= "DROP"
			// increment drop counter
			guardian.publisher.IncrDropCounter(eTLDPlusOne,"")
			// publish drop for supervision
			guardian.publisher.PublishDrop(eTLDPlusOne)
			break
		}

		//if verdict == "DROP" {
		//	break
		//}

	}
	return verdict
}


