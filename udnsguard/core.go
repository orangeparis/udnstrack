package dnsguard

import (
	"github.com/kung-foo/freki/netfilter"
	"os"
	"log"
	"os/signal"
	"time"
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
)

/*
	udnsguard.Guardian


	read nfqueue  ( dns traffic )

	unpack dns packet
		extract domain ( etld_plus_one )
		increment domain counter via store.IncrDomainCounter

		check blacklist  ( store.IsInBlacklist )
		drop packet if domain in blacklist


 */


func onErrorExit(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func onInterruptSignal(fn func()) {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)

	go func() {
		<-sig
		fn()
	}()
}

// iptables -A INPUT --dport dns -j NFQUEUE --queue-num 0
// iptables -A OUTPUT --sport dns -j NFQUEUE --queue-num 0

// a redis pubsub publisher
type Guardian struct {

	nfqueue * netfilter.Queue
	counter uint
	publisher * dnsbroker.Store
}


func NewGardian() *Guardian {


	log.Printf("create new guardian\n")

	nfqueue, err := netfilter.New(0, 100, netfilter.NF_DEFAULT_PACKET_SIZE)
	onErrorExit(err)

	//onInterruptSignal(func() {
	//
	//	nfqueue.Close()
	//	log.Printf("\n%d packets processed", pp)
	//	os.Exit(0)
	//})

	//hive := dnsbroker.NewHive(name,url)
	publisher := dnsbroker.NewStore()

	guardian := Guardian{nfqueue,0,publisher}


	onInterruptSignal(func() {
		guardian.Close()
		//nfqueue.Close()
		//log.Printf("\n%d packets processed", pp)
		//os.Exit(0)
	})


	return &guardian
}


func (guard * Guardian) Close() {

	guard.nfqueue.Close()
	guard.publisher.Close()
	log.Printf("Guardian closed: %d packets processed\n", guard.counter)
	os.Exit(-1)

}

func (guard * Guardian) Start() {

	// start nfqueue
	log.Printf("starting nfqueue\n")
	go guard.nfqueue.Run()
	log.Printf("nfqueue started\n")


	//packets := guard.nfqueue.Packets()

	guard.HandleDns()



}

func (guard * Guardian) HandleDns() {


	log.Printf("Handle Dns")
	// main loop
	pChan := guard.nfqueue.Packets()

	log.Printf("entering main HandleDns loop\n")

	for p := range pChan {
		//log.Printf("packet received here\n")

		//spew.Dump(p)
		// p is a netfilter rawPacket
		// p.ID is  netfilter packet id to set verdict on
		// p.Data is the raw content (ip layer) of the received packet )

		log.Printf("==================== PKT [%03d] \n", p.ID)
		// fmt.Printf("------------------------------------------------------------------------\n id: %d\n", payload.Id)
		// fmt.Println(hex.Dump(payload.Data))
		// Decode a packet

		event, err := guard.Filter(p)
		//println("dissector result:",err,"\n")
		if err != nil {
			println("filter error result:", err, "\n")
		} else {
			// success we have a dns event
			//spew.Dump(event)
		}

		if event["_verdict"] == "DROP" {
			log.Printf("==================== DROP PACKET: [%03d]", p.ID)
			guard.nfqueue.SetVerdict(p, netfilter.NF_DROP)
		} else {
			log.Printf("===================== ACCEPT PACKET: [%03d]", p.ID)
			guard.nfqueue.SetVerdict(p, netfilter.NF_ACCEPT)
		}
		guard.counter++
		// tempo to slow down
		time.Sleep(1 * time.Millisecond)
	}

	log.Printf("no more packets from nfqueue \n")

}



//
//func main() {
//
//	// create Hive singleton for dnsbroker
//	dnsbroker.NewHive("dnsguard","redis://redis:6379/0")
//
//	guard := NewGardian()
//	guard.Start()
//
//
//	//q, err := netfilter.New(0, 100, netfilter.NF_DEFAULT_PACKET_SIZE)
//	//onErrorExit(err)
//	//
//	////pp := 0
//	//
//	//onInterruptSignal(func() {
//	//	q.Close()
//	//	log.Printf("\n%d packets processed", pp)
//	//	os.Exit(0)
//	//})
//	//
//	//
//	//Handle_dns(q)
//
//}

