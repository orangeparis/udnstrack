package bigrams


import (

	"testing"
	"log"

	"time"

	//"github.com/magiconair/properties/assert"
)


func TestFrequencyTable( t *testing.T) {

	// build a bigram with alexa domains

	start := time.Now()
	bigrams, _ := NewBigramIndex()
	// load 100 000 entries
	err := LoadBigramsFromFile(bigrams, top_count)
	if err != nil {
		log.Fatal(err)
	}
	elapsed := time.Since(start)
	log.Printf("build bigram for %d entries took %s", top_count, elapsed)



	tbl := NewFrequencyTable("Top 100 000 domains from Alexa list", bigrams)


	f1 := tbl.Frequency("go")
	_=f1


	ranks := tbl.ByRank()
	for _,bigram := range ranks {

		freq := tbl.Frequency(bigram)
		occ := tbl.Occurence(bigram)

		log.Printf("%s: %f  (%d)\n",bigram,freq,int(occ))
	}

}




