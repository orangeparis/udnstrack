package bigrams

import (

	"strings"

)


type DomainScorer struct {

	Name string

	levels []string

}


func ( d *DomainScorer) Score ( reference * BigramIndex) (score float64){

	// split domain
	d.levels = strings.Split( d.Name, ".")

	stats := &Stats{}
	reference.ComputeStats(stats)
	total := stats.Total

	var FreqSum float64
	var divisor float64

	for i,domain := range d.levels {

		if i <= len(d.levels)-2 {

			divisor += float64( len(domain) + 2 -1)
			for _,gram := range ParseBigram(domain) {

					occurence := reference.Index[gram]
					Freq := float64(occurence) / float64(total)
					FreqSum += Freq
			}
		}

	}
	score = FreqSum / divisor
	return score

}


type Stats struct {

	Ready bool

	Min int
	Max int
	Count int64       // number of bigram entries
	Total int64     // total occurences

	//Mean int		// moyenne

}


func ( s * Stats) Add( value int  ) (){

	s.Count += 1
	s.Total += int64(value)
	if s.Min == 0  || value < s.Min {
		s.Min = value
		//s.MinBigram = bigram
	}
	if value > s.Max {
		s.Max= value
		//s.MaxBigram = bigram
	}
}


func ( s * Stats) Mean() (moy float64){
	return float64(s.Total) / float64(s.Count)
}


func ( s * Stats) Moy() (moy float64){
	return float64(s.Total) / float64(s.Count)
}