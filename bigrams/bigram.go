package bigrams


/*

	EXPERIMENTAL

	qualify domain names with bigram

	see: https://core.ac.uk/download/pdf/82291078.pdf
	and: https://github.com/Lazin/go-ngram/blob/master/ngram.go

	see also: .Alexa.com. Alexa top 1,000,000 global sites. http://www.alexa.com/topsites, June 2012.

 */


import (
	//"fmt"
	//"math"
	"strings"
	//"unicode"
	"sync"
)

const (

	defaultPad = "$"

)


type bigramHandler interface {

	Add (string) error
	AddDomain ( string) error
}



//type nGramValue map[string]int
type bigramValue map[string]int


// SearchResult contains token id and similarity - Value in range from 0.0 to 1.0
type SearchResult struct {
	//TokenID    TokenID
	Similarity float64
}



// NGramIndex can be initialized by default (zeroed) or created with "NewNgramIndex"
type BigramIndex struct {

	Pad   string
	//N     int
	//spool stringPool
	Index bigramValue

	total int64

	warp  float64

	sync.RWMutex
}


func NewBigramIndex() (*BigramIndex, error) {
	ngram := new(BigramIndex)
	ngram.init()
	return ngram, nil
}


func (ngram *BigramIndex) init() {
	ngram.Lock()
	defer ngram.Unlock()

	ngram.Index = make(bigramValue)
	if ngram.Pad == "" {
		ngram.Pad = defaultPad
	}
}


func (ngram *BigramIndex) Add(input string) ( err error) {
	// add string to
	if ngram.Index == nil {
		ngram.init()
	}

	bigrams := ParseBigram(input)
	for _,bigram := range bigrams {
		ngram.Index[bigram]++
	}

	return err
}


func (ngram *BigramIndex) ComputeStats ( stats * Stats) {

	// compute and update the total of occurence

	ngram.RLock()
	for bigram, occurence := range ngram.Index {

		_ = bigram
		stats.Add(occurence)
	}
	ngram.total = stats.Total
	ngram.RUnlock()
	return
}

func (ngram *BigramIndex) AddDomain(input string) ( err error) {
	// add domain to bigram ( ignore TLD )

	// split domain
	domains := strings.Split( input, ".")
	for i,domain := range domains {
		if i <= len(domains) -2  {
			err = ngram.Add(domain)
			if err != nil {
				return err
			}
		}
	}
	return err
}


func (ngram *BigramIndex) Frequency( bigram string) ( frequency float64 ) {
	// compute frequency : nb of occurence of the bigram / total of occurences

	var total int64 = 0

	stats := &Stats{}

	// split domain
	occurence,ok := ngram.Index[bigram]
	if ok {
		ngram.ComputeStats( stats)
		if stats.Total != 0 {
			frequency = float64(occurence) / float64(total)
		}
	}
	return frequency
}

//func (ngram *BigramIndex) Score( bigram  BigramIndex) ( score float64 ) {
//	// compute the score of the bigrams toward a bigram Index reference
//
//	// for each internal bigram
//	stats := &Stats{}
//	bigram.ComputeStats(stats)
//	total := stats.Total
//
//	var FreqSum float64
//
//	for gram,occ := range ngram.Index {
//
//		_= occ
//		occurence := bigram.Index[gram]
//		Freq := float64(occurence) / float64(total)
//		FreqSum += Freq * float64(occ)
//	}
//
//	score = FreqSum / float64( len("google") + 2 -1)
//
//	return score
//}





//func (ngram *BigramIndex) match(input string, tval float64) ([]SearchResult, error) {
//	//inputNgrams, error := ngram.splitInput(input)
//	//if error != nil {
//	//	return nil, error
//	//}
//	output := make([]SearchResult, 0)
//	tokenCount := ngram.countNgrams(inputNgrams)
//	for token, count := range tokenCount {
//		var sim float64
//		allngrams := float64(len(inputNgrams))
//		matchngrams := float64(count)
//		if ngram.warp == 1.0 {
//			sim = matchngrams / allngrams
//		} else {
//			diffngrams := allngrams - matchngrams
//			sim = math.Pow(allngrams, ngram.warp) - math.Pow(diffngrams, ngram.warp)
//			sim /= math.Pow(allngrams, ngram.warp)
//		}
//		if sim >= tval {
//			res := SearchResult{Similarity: sim, TokenID: token}
//			output = append(output, res)
//		}
//	}
//	return output, nil
//}




//// SplitOnNonLetters splits a string on non-letter runes
//func SplitOnNonLetters(s string) []string {
//	notALetter := func(char rune) bool { return !unicode.IsLetter(char) }
//	return strings.FieldsFunc(s, notALetter)
//}
//
//var str = "This is a 'sentence' about this thing I wrote. I wrote it yesterday."
//
//func do() {
//
//	str = strings.ToLower(str)
//	parts := SplitOnNonLetters(str)
//	fmt.Printf("%+v\n", parts)
//
//	fmt.Println(ngrams(parts, 2))
//	fmt.Println(ngrams(parts, 3))
//}
//
//func ngrams(words []string, size int) (count map[string]uint32) {
//
//	count = make(map[string]uint32, 0)
//	offset := int(math.Floor(float64(size / 2)))
//
//	max := len(words)
//	for i  := range words {
//		if i < offset || i+size-offset > max {
//			continue
//		}
//		gram := strings.Join(words[i-offset:i+size-offset], " ")
//		count[gram]++
//	}
//
//	return count
//}


//func ParseBigram( word string  ) (result []string) {
//	// parse a string return an array of bigram
//	//  eg google =>  [ $g , go , oo , og , gl , le , e$ ]
//
//	word = defaultPad + word + defaultPad
//
//	letters := []rune(word)
//	max := len(letters) -1
//	//gram := ""
//	for i := range letters  {
//		if i >= max {
//			break
//		}
//		gram := string(letters[i])
//		gram += string(letters[i+1])
//		result = append(result,gram)
//	}
//	return result
//}



func ParseBigram( word string  ) (result []string) {
	// parse a string return an array of bigram
	//  eg google =>  [ $g , go , oo , og , gl , le , e$ ]

	word = defaultPad + word + defaultPad
	runes := []rune(word)

	max := len(word) -1
	//gram := ""
	for i := range word  {
		if i >= max {
			break
		}
		result = append(result,string(runes[i:i+2]))
	}
	return result
}