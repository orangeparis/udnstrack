package bigrams



import (

	"testing"
	//"os"
	"log"
	//"fmt"
	//"strings"

	"time"
	//"bufio"

	"github.com/magiconair/properties/assert"

)


var (

	//top_source = "../assets/top-1m.csv"
	//top_count = 100000
)



//func LoadBigramsFromFile( bigram * BigramIndex, limit int) (err error) {
//
//	if limit == 0 {
//		limit = top_count
//	}
//
//
//	file, err := os.Open(top_source)
//	if err != nil {
//		//log.Fatal(err)
//		return err
//	}
//	defer file.Close()
//
//	counter := 0
//	scanner := bufio.NewScanner(file)
//	for scanner.Scan() {
//		line := scanner.Text()
//		//fmt.Println(line)
//		counter += 1
//		if counter > limit {
//			break
//		}
//		// split indice / domain name
//		s := strings.Split(line, ",")
//		if len(s) >= 2 {
//			domain := s[1]
//			//fmt.Println(domain)
//
//			// compute bigram
//			bigram.AddDomain(domain)
//		}
//	}
//	if err := scanner.Err(); err != nil {
//		return err
//	}
//	return err
//}
//



func TestParseBigram ( t *testing.T) {


	grams := ParseBigram("google")
	assert.Equal(t,len(grams),7)
	assert.Equal(t,grams[0],"$g")
	assert.Equal(t,grams[6],"e$")

	grams = ParseBigram("world")
	assert.Equal(t,len(grams),6)
	assert.Equal(t,grams[0],"$w")
	assert.Equal(t,grams[5],"d$")

	grams = ParseBigram("X")
	assert.Equal(t,len(grams),2)
	assert.Equal(t,grams[0],"$X")
	assert.Equal(t,grams[1],"X$")

	grams = ParseBigram("")
	assert.Equal(t,len(grams),1)

	return

}


func TestBenchBigram ( t *testing.T) {

	iterations := 100000

	start := time.Now()
	for i:= 0 ; i < iterations ; i++ {
		grams := ParseBigram("google")
		assert.Equal(t, len(grams), 7)
		assert.Equal(t, grams[0], "$g")
		assert.Equal(t, grams[6], "e$")
	}
	elapsed := time.Since(start)
	log.Printf("bench ParseBigram for %d entrries took %s", iterations, elapsed)

	return

}


//func TestBenchBigram2 ( t *testing.T) {
//
//	iterations := 100000
//
//	start := time.Now()
//	for i:= 0 ; i < iterations ; i++ {
//		grams := udnswatch.ParseBigram2("google")
//		assert.Equal(t, len(grams), 7)
//		assert.Equal(t, grams[0], "$g")
//		assert.Equal(t, grams[6], "e$")
//	}
//	elapsed := time.Since(start)
//	log.Printf("bench ParseBigram2 for %d entrries took %s", iterations, elapsed)
//
//	return
//
//}






func TestNgramBasic ( t *testing.T) {

	//var words []string{"hello", "world" }


	n,_ := NewBigramIndex()


	n.Add("hello")
	n.Add("world")
	n.Add("hel")


	n.AddDomain("google.com")
	n.AddDomain("orange.uk")

	assert.Equal(t, n.Index["he"], 2)
	assert.Equal(t, n.Index["el"], 2)


	n.AddDomain("x64tgSoPvvIK2adKaH-Hr.xqMkCXQg.orangedns.info")
	return

}



func TestBigramOnTopDomains( t *testing.T) {


	start := time.Now()

	// create bigram Index
	bigram,_ := NewBigramIndex()

	// load 100 000 entries
	err := LoadBigramsFromFile( bigram, top_count)
	if err != nil {
		log.Fatal(err)
	}

	elapsed := time.Since(start)
	log.Printf("build bigram for %d entries took %s", top_count, elapsed)

}



//func TestScoreOnTopDomains( t *testing.T) {
//
//	start := time.Now()
//
//	// create bigram Index for the dns database
//	bigram,_ := NewBigramIndex()
//
//	// load 100 000 entries
//	err := LoadBigramsFromFile( bigram, top_count)
//	if err != nil {
//		log.Fatal(err)
//	}
//
//
//	elapsed := time.Since(start)
//	log.Printf("build bigram for %d entries took %s", top_count, elapsed)
//
//
//
//	stats := & Stats{}
//	bigram.ComputeStats(stats)
//	total:= stats.Total
//	nbigram := stats.Count
//	min := stats.Min
//	max := stats.Max
//	moy := stats.Moy()
//
//	_=nbigram
//	_=total
//	_=min
//	_=max
//	_=moy
//
//	//r1 := bigram.Index["s$"]
//	//f1 := float64(r1) / float64(stats.Total)
//	//_=r1
//	//_=f1
//
//	most_used := []string{"s$","in","$c","er","co","an","re","ar","es"}
//
//	for _,bi := range most_used {
//		r := bigram.Index[bi]
//		f := float64(r) / float64(stats.Total)
//		fmt.Printf("bigram: %s, occ= %d , freq=%f\n",bi,r,f)
//	}
//
//
//	// create a bigram Index for a domain
//	domain := &DomainScorer{Name:"google.com",Reference: *bigram}
//	score := domain.Score( bigram.Index )
//	_=score
//
//
//	denise := &udnswatch.DomainScorer{Name:"x64tgSoPvvIK2adKaH-Hr.xqMkCXQg.orangedns.info",Reference: *bigram}
//	score_denise := denise.Score()
//	_=score_denise
//
//
//	orange := &udnswatch.DomainScorer{Name:"orange.com",Reference: *bigram}
//	score_orange := orange.Score( )
//	_=score_orange
//
//
//	return
//
//
//}