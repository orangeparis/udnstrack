package bigrams


import (

	"strings"
	"golang.org/x/net/publicsuffix"
	"log"
)


type DomainRepository struct {

	// a set of domains to compute stats

	Name string   // description of the domain sets eg:  100 000 alexa domains

	Domains <- chan string  // list of domains  eg google.com ,

	FrequencyTable * FrequencyTable

	Ready bool
	Stats Stats

}


func NewDomainStats ( name string ,domains <-chan string, frequencyTable * FrequencyTable)  * DomainRepository{

	d := &DomainRepository{Name:name, Domains: domains,FrequencyTable:frequencyTable,Ready:false}

	return d

}


func ( d *DomainRepository) Compute ( pq PriorityQueue) {

	/*

	 */

	i:=0

	for domain := range d.Domains {


		mean_domain := domain
		var score float64 = 1

		// get etld plus one
		eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(domain, "."))
		if err != nil {
			log.Printf("%s\n",err.Error())
			mean_domain = domain

		} else {
			if len(domain) > len(eTLDPlusOne) {
				// the domain is longer than
				mean_domain = domain[: len(domain)-len(eTLDPlusOne)-1] + ".nul"
			}
		}

		//origin_score := d.Score(domain)
		//_=origin_score
		score = d.Score(mean_domain)
		_=score

		pq[i] = &Item{
					Value:    domain,
					Priority: int(1/score),
					Index:    i,
		}
		i++

	}

	d.Ready = true

}



func ( d *DomainRepository) Score (domain string) (score float64) {


	// init heap queue

	// split domain
	levels := strings.Split( domain, ".")

	var FreqSum float64
	var divisor float64

	for i,part := range levels {

		if i <= len(levels)-2 {

			divisor += float64( len(part) + 2 -1)
			for _,gram := range ParseBigram(part) {

				frequency := d.FrequencyTable.Frequency(gram)
				FreqSum += frequency
			}
		}

	}
	score = FreqSum / divisor
	return score
}