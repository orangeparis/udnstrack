package bigrams

import (
	"os"
	"bufio"
	"strings"
)

var (

	top_source = "../assets/top-1m.csv"
	top_count = 100000
)



func LoadBigramsFromFile( bigram * BigramIndex, limit int) (err error) {
	/*
		feed bigram with domains from the alexa domain list

	 */

	if limit == 0 {
		limit = top_count
	}


	file, err := os.Open(top_source)
	if err != nil {
		//log.Fatal(err)
		return err
	}
	defer file.Close()

	counter := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		//fmt.Println(line)
		counter += 1
		if counter > limit {
			break
		}
		// split indice / domain name
		s := strings.Split(line, ",")
		if len(s) >= 2 {
			domain := s[1]
			//fmt.Println(domain)

			// compute bigram
			bigram.AddDomain(domain)
		}
	}
	if err := scanner.Err(); err != nil {
		return err
	}
	return err
}





