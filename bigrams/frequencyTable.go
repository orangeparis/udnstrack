package bigrams


import(

	"container/heap"
)



type FrequencyTable struct {

	Name string   // generic name of the frequency table
	//Entries int   // numbers of domains used to build the table (10 000 , 1000 000)

	Bigram * BigramIndex

	//Count int  			// number of bigrams
	//Occurences float64 	// total number of bigram occurences


	Stats * Stats       // stats of the table ( min , max , mean ...

	//PriorityQueue * PriorityQueue

}

func NewFrequencyTable( name string , index * BigramIndex) ( table * FrequencyTable) {

	stats := &Stats{ Ready:false}

	table = &FrequencyTable { Name:name , Bigram: index, Stats: stats}

	table.init()

	return table
}

func (f * FrequencyTable) init() {
	// compute stats for the table

	for _,occurence := range f.Bigram.Index {

		f.Stats.Add(occurence)

	}
	f.Stats.Ready = true

}


func (f * FrequencyTable) Rank ( bigram string) (rank int) {
	// return the rank of the bigram
	return f.Bigram.Index[bigram]
}

func (f * FrequencyTable) Occurence ( bigram string) ( occurence float64) {
	// return the number of occurence of a bigram
	return float64(f.Bigram.Index[bigram])
}


func (f * FrequencyTable) Frequency ( bigram string) ( freq float64 ) {
	// return the frequency  ( occurence of the bigram / total occurences
	occ := float64(f.Bigram.Index[bigram])
	freq = occ / float64(f.Stats.Total)
	return freq
}

func  (f * FrequencyTable) ByRank() []string {
	// return a list of bigram names ordered by rank
	var l []string

	pq := make(PriorityQueue, len(f.Bigram.Index))
	i := 0
	for value, priority := range f.Bigram.Index {
		pq[i] = &Item{
			Value:    value,
			Priority: priority,
			Index:    i,
		}
		i++
	}
	heap.Init(&pq)

	// Take the items out; they arrive in decreasing Priority order.
	for pq.Len() > 0 {
		item := heap.Pop(&pq).(*Item)
		l = append(l,item.Value)
	}

	return l
}



