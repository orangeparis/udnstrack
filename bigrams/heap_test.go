package bigrams

import (

	"testing"
	"log"
	//"fmt"

	"time"


	//"github.com/magiconair/properties/assert"

	"container/heap"
)




/*




 */




func TestHeap( t *testing.T) {


	// build a bigram with alexa domains

	start := time.Now()

	// create bigram Index
	bigrams,_ := NewBigramIndex()

	// load 100 000 entries
	err := LoadBigramsFromFile( bigrams, top_count)
	if err != nil {
		log.Fatal(err)
	}

	elapsed := time.Since(start)
	log.Printf("build bigram for %d entries took %s", top_count, elapsed)



	// build the heap
	// Create a Priority queue, put the items in it, and
	// establish the Priority queue (heap) invariants.
	pq := make(PriorityQueue, len(bigrams.Index))
	i := 0
	for value, priority := range bigrams.Index {
		pq[i] = &Item{
			Value:    value,
			Priority: priority,
			Index:    i,
		}
		i++
	}
	heap.Init(&pq)


	// Take the items out; they arrive in decreasing Priority order.
	for pq.Len() > 0 {
		item := heap.Pop(&pq).(*Item)
		log.Printf("%.2d:%s ", item.Priority, item.Value)
	}


}
