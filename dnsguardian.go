package main

// iptables -A INPUT --dport dns -j NFQUEUE --queue-num 0
// iptables -A OUTPUT --sport dns -j NFQUEUE --queue-num 0

import (
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"bitbucket.org/orangeparis/udnstrack/filePlayer"
	"bitbucket.org/orangeparis/udnstrack/metrics"
	"bitbucket.org/orangeparis/udnstrack/netfilterPlayer"
	"bitbucket.org/orangeparis/udnstrack/server"
	"bitbucket.org/orangeparis/udnstrack/udnswatch"
	"log"
	"time"

	//"bitbucket.org/orangeparis/udnstrack/metrics"
	"sync"
	//"os"
	//"time"
	//"fmt"
	"bitbucket.org/orangeparis/udnstrack/proxy"
	"context"
	"fmt"
)

var (
	name = "hive"
	url  = "redis://redis:6379/0"

	wg sync.WaitGroup
)

type Object struct {
	//data
}

// a coroutine for a guardian file player ( for local testing purpose )
func GuardianFilePlayerTask(wg *sync.WaitGroup) {
	// for testing purposes
	log.Printf("starting dnsguardian with netfilter\n")
	guardian, _ := filePlayer.NewPlayer("/Users/cocoon/tmp/app-traffic.pcap")
	defer guardian.Close()
	guardian.Start()
	wg.Done()
	return
}

// a coroutine for a net filter guardian
func GuardianNetFilterTask(wg *sync.WaitGroup) {
	//
	log.Printf("starting dnsguardian with netfilter\n")
	guardian, _ := netfilterPlayer.NewNetFilterPlayer()
	defer guardian.Close()
	guardian.Start()
	wg.Done()
	return
}

// a coroutine for a watcher
func WatcherTask(wg *sync.WaitGroup) {
	//
	log.Printf("starting dnswatcher")
	watcher := udnswatch.NewWatcher()
	defer watcher.Close()
	watcher.Run()
	wg.Done()
	return
}

// a coroutine for a httpserver
func ServerTask(wg *sync.WaitGroup) {
	//
	log.Printf("starting http server")
	server.Run()
	wg.Done()
	return
}

func PushToGatewayTask(wg *sync.WaitGroup, cnf *dnsbroker.DnstrackConfig) {
	//
	log.Printf("starting prometheus PushToGateway\n")

	source := "http://localhost:3333/metrics" // local server to get prometheus metrics
	target := cnf.Backend.PrometheusUrl       //  "192.168.99.100:9091"
	job := "dnsguard"
	instance := "demo"

	push_timer := 5

	exporter := metrics.NewExporter(source, target, job, instance)

	ctx := context.TODO()
	exporter.Run(ctx, push_timer)
	wg.Done()
	return
}

func UpdateBackendConfigTask(wg *sync.WaitGroup, cnf *dnsbroker.DnstrackConfig) {
	//
	log.Printf("starting Update Backend config\n")

	// wait for server up
	//time.Sleep(5 * time.Second)

	store := dnsbroker.NewStore()

	//source := "http://localhost:3333/api/v1/agents/demo/config" // backend config
	source := cnf.Backend.Master
	// authorization header  eg Basic XXXXXXXX
	token := cnf.Backend.MasterToken
	// probe id ( eg DEMO )
	probe_id := cnf.Backend.ProbeId
	refresh := cnf.Backend.PollConfigFrequency

	updater := proxy.NewUpdater(source, token, probe_id, refresh, store)
	ctx := context.TODO()

	updater.Run(ctx)
	wg.Done()
	return
}

func main() {

	// setup the context

	// create the configuration singleton   ( task will ask dnsbroker.GetConfig() )
	conf := dnsbroker.NewConfig("./dnstrak.toml")

	// create the hive singleton for redis Clients  ( task will ask dnsbroker.)
	log.Printf("create redis hive at %s", conf.Redis.Url)
	dnsbroker.NewHive(name, conf.Redis.Url)

	// setup the prometheus metrics recorder
	metrics.InitMetric()

	// get the context
	//cnf := dnsbroker.GetConfig()

	// set the wait group
	wg.Add(5)

	// start the coroutines

	go WatcherTask(&wg)
	go ServerTask(&wg)

	// launch guardian task
	//go GuardianFilePlayerTask(&wg)
	go GuardianNetFilterTask(&wg)

	time.Sleep(5 * time.Second)

	// start push to prometheus gateway
	go PushToGatewayTask(&wg, conf)

	// start backend config updater
	go UpdateBackendConfigTask(&wg, conf)

	// wait for task to end
	wg.Wait()

	fmt.Println("Group done")

}
