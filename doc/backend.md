the backend holds the configuration file




api

# get the config ( rules.json)

GET https://<backend_host>/config 


## config stucture

    {
      "config":
        {
          "get_config_frequency ": 300,
          "simulation_mode": True,
          "push_detected_domains_frequency ": 60
       },
    
      "rules": [
        {
          "field_label": "TXT",
          "rule_label": "txt_rule",
          "max_length": 0
        },
        {
          "field_label": "NULL",
          "rule_label": "null_rule",
          "max_length": 10
       }
     ],
    
      "blacklist": [
        "freedom.net",
        "12345azer.de"
      ],
    
      "whitelist": [
        "google.com",
        "orange.fr"
      ]
    }
