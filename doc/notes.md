
udnstrack solution
==================

a minimal solution to track dns tunnel

* intercept traffic on udp 53 and decide to forward the dns packet or drop it
* increment counter in redis database for the etld_plus_one
* check redis database for blacklisted
* if domain is in blacklist drop the packet and increment drop counter
* pass it to the unbound dns server otherwise

#components:


##udnsguard


* intercept traffic on udp port 53
* dissect dns packet to extract the primary domain (etld_plus_one)
* increment a hit counter for this etld_plus_one  ( redis key hit:<domain>)
* check in db if domain is in blacklist   ( redis blacklist:<domain>)
* drop packet if domain is blacklisted and increment drop counter drop:<domain>
* pass packet to dns server otherwise


##udnswatch


* periodicaly check all domain counters  ( redis key hit:*)
* for each domain not in the WHITELIST, 

   if count > LIMIT  : add domain to blacklist for a period of BLACKLIST_TTL 
   
   (redis key blacklist:<domain>)


##dnsbroker

* common api to access redis database
  * increment domain counter   hit:<domain>
  * check blacklist            blacklist:<domain>
  * check whitelist            whitelist:<domain>
* handle the broker (redis)

subcomponents:
* redis server



#Glossary

## eTLD
eTLD is for effective top-level domain

we need to extract the etld + 1 of a dns request 
in order to increment a counter for this domain

the extraction use the go package: golang.org/x/net/publicsuffix

the package use a public suffix list (maintain by mozilla)

publicsuffix url to get the list: 

    https://publicsuffix.org/list/effective_tld_names.dat


# Unbound Dns

## 10 Reasons to use Unbound DNS :

    http://info.menandmice.com/blog/bid/37244/10-Reasons-to-use-Unbound-DNS
    
    
## Configuring unbound as a local DNS server

    https://www.asrivas.me/blog/configuring-unbound-as-a-local-dns-server/
    
    
# useful links

detect domain names with high entropy (probably encrypted traffic)

    https://is.muni.cz/th/p7gwp/text_prace.pdf
    

detecting dns tunnels

    https://www.giac.org/paper/gcia/1116/detecting-dns-tunneling/108367