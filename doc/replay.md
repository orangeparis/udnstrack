# purpose

replay dns trafic



## capture trafic to a pcap sudo 

    tcpdump -vvv -i en0 host 192.168.1.26 and port 53 -w app-traffic.pcap &
    
    
## view it

    tcpdump -qns 0 -X -r app-traffic.pcap
    

## adapt with tcp rewrite (if necessary)

    tcprewrite --pnat=10.0.0.0/8:172.16.0.0/12,192.168.0.0/16:172.16.0.0/12 --infile=input.pcap --outfile=output.pcap --skipbroadcast
    
    


## replay it

    