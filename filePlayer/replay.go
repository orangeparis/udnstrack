package filePlayer

/*


	filter dns packets comming from a pcap file

*/

import (
	"log"
	"os"
	//"os/signal"
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"bitbucket.org/orangeparis/udnstrack/filters"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"time"
)

// a redis pubsub publisher
type Player struct {
	publisher *dnsbroker.Store
	filter    *filters.Filter
	handle    *pcap.Handle
	counter   int
}

func NewPlayer(pcapFile string) (player *Player, err error) {

	var handle *pcap.Handle

	log.Printf("create new file player\n")

	player = &Player{}

	// Open file instead of device
	handle, err = pcap.OpenOffline(pcapFile)
	if err != nil {
		return player, err
	}

	//hive := dnsbroker.NewHive(name,url)
	publisher := dnsbroker.NewStore()
	filter := filters.NewFilter(publisher)

	player = &Player{
		filter:    filter,
		publisher: publisher,
		handle:    handle,
	}

	return player, err
}

func (player *Player) Close() {
	//
	player.publisher.Close()
	player.handle.Close()
	log.Printf("Guardian closed: %d packets processed\n", player.counter)
	os.Exit(-1)

}

func (player *Player) Start() {

	player.HandleDns()

}

func (player *Player) HandleDns() {

	log.Println("Handle Dns")
	// main loop

	log.Printf("entering main HandleDns loop\n")

	var packetId uint32

	// Loop through packets in file
	packetSource := gopacket.NewPacketSource(player.handle, player.handle.LinkType())

	packetId = 0 // (p.id )
	for p := range packetSource.Packets() {

		//spew.Dump(p)

		player.HandlePacket(p, packetId)
		player.counter++
		packetId++
		// tempo to slow down
		time.Sleep(1 * time.Millisecond)
	}

	log.Printf("no more packets from source \n")

}

func (player *Player) HandlePacket(packet gopacket.Packet, id uint32) {

	//log.Printf("==================== PKT [%03d] \n", id)

	event, err := player.filter.Filter(packet, id)
	if err != nil {
		// unknown decode error
		log.Printf("filter error result: %s\n", err)
		// in doubt let it pass
		event["_verdict"] = "PASS"
	} else {
		// success we have a dns event
		//spew.Dump(event)
	}

	if event["_verdict"] == "DROP" {
		log.Printf("==================== DROP PACKET: [%03d]", id)
	} else {
		log.Printf("===================== ACCEPT PACKET: [%03d]", id)

	}
	player.counter++
	// tempo to slow down
	time.Sleep(1 * time.Millisecond)
}
