package filePlayer_test



import (
	"testing"
	"github.com/google/gopacket/pcap"
	"log"
	"bitbucket.org/orangeparis/udnstrack/filePlayer"
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"bitbucket.org/orangeparis/udnstrack/metrics"
)

var (
	pcapFile string = "/Users/cocoon/tmp/app-traffic.pcap"
	handle   *pcap.Handle
	err      error
)

var HIVE_NAME = "hive"
var REDIS_URL = "redis://localhost:6379/0"
//var REDIS_URL = "redis://192.168.99.100:6379/0"


func newHive() * dnsbroker.Hive {

	name := HIVE_NAME
	url := REDIS_URL

	dnsbroker.ResetHive()

	h := dnsbroker.NewHive(name,url)
	hive := dnsbroker.GetHive()

	println(hive==h)

	return hive

}




func TestPlayer( t *testing.T) {

	// open a pcap file and handle packet with player

	// create hive for broker
	newHive()
	// setup the prometheus metrics recorder
	metrics.InitMetric()



	player ,err := filePlayer.NewPlayer(pcapFile)
	if err != nil {
		log.Printf("error: %s",err.Error())
		return
	}
	player.Start()


	return


}
