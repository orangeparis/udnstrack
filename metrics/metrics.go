package metrics

/*

	define prometheus metrics


	samples


*/

import (

	//"fmt"
	//"time"
	"os"
	//"runtime"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
	"log"
	"sync"
)

/// declare metrics

// global Metric singleton
var (
	Metric *DnsguardMetrics
)

var gatewayUrl = "http://my-pushgateway-vm1:9091/"
var push_job_name = "job_push"

type DnsguardMetrics struct {
	sync.Mutex

	Name string // name of the dnsguard server
	//Started time

	Gateway string // gateway url for pushing
	pusher  *push.Pusher
}

func InitMetric() {

	hostname, _ := os.Hostname()
	Metric = &DnsguardMetrics{sync.Mutex{}, hostname, gatewayUrl, nil}
	Metric.InitMetrics()

}

func (e *DnsguardMetrics) InitMetrics() {

	e.Lock()
	defer e.Unlock()
	prometheus.MustRegister(dropCounter)
	prometheus.MustRegister(hitCounter)
	prometheus.MustRegister(blackListCounter)
	//prometheus.MustRegister(goroutinesGauge)

	//prometheus.Register(histogram)
	//prometheus.MustRegister(curlErrorCollector)

	// initialize the pusher ( to external push gateway )
	registry := prometheus.DefaultGatherer
	e.pusher = push.New(e.Gateway, push_job_name).Gatherer(registry)

}

var dropCounter = prometheus.NewCounterVec(

	prometheus.CounterOpts{
		Name: "drop_counter",
		Help: "number of packets dropped by dnsguard",
	},
	[]string{"etld_p1"},
)

var hitCounter = prometheus.NewCounterVec(

	prometheus.CounterOpts{
		Name: "hit_counter",
		Help: "number of dns questions received",
	},
	[]string{"etld_p1", "domain"},
)

var blackListCounter = prometheus.NewCounterVec(

	prometheus.CounterOpts{
		Name: "blacklist_counter",
		Help: "number of blacklisted domains",
	},
	[]string{"etld_p1"},
)

// push functions ( not yet used : use exporter instead
// metrics recorders
func (e *DnsguardMetrics) PushDropIncrement(etld_p1 string) {
	e.Lock()
	defer e.Unlock()
	dropCounter.With(prometheus.Labels{"etld_p1": etld_p1}).Inc()

	err := e.pusher.Push()
	if err != nil {
		log.Printf("cannot push to %s: %s", e.Gateway, err.Error())
	}

}

//var goroutinesGauge = prometheus.NewGaugeVec(
//	prometheus.GaugeOpts{
//	Name: "my_app_goroutines_count",
//	Help: "number of goroutines that currently exist",
//	},
//	[]string{"hostname"},
//	)

//var histogram = prometheus.NewHistogramVec(	prometheus.HistogramOpts{
//
//	Name: "hash_duration_seconds",
//	Help: "Time taken to create hashes",
//
//	}, []string{"code"})
//
//
//var curlErrorCollector = prometheus.NewCounterVec(
//
//	prometheus.CounterOpts{
//	Name: "error_curl_total",
//	Help: "Total curl request failed",
//	},
//	[]string{"vendor"},
//	)
//
//

// metrics recorders
func (e *DnsguardMetrics) RecordDropIncrement(etld_p1 string) {
	e.Lock()
	defer e.Unlock()
	dropCounter.With(prometheus.Labels{"etld_p1": etld_p1}).Inc()

}

func (e *DnsguardMetrics) RecordHitIncrement(etld_p1, domain string) {
	e.Lock()
	defer e.Unlock()
	hitCounter.With(prometheus.Labels{"etld_p1": etld_p1, "domain": domain}).Inc()
}

func (e *DnsguardMetrics) ResetHitCounter(etld_p1 string) {
	// remove all counters for this etld_plus one
	//hitCounter.With(prometheus.Labels{"etld_p1": etld_p1 , "domain": domain}).Inc()
	e.Lock()
	defer e.Unlock()
	hitCounter.Reset()

}

func (e *DnsguardMetrics) RecordBlackListIncrement(etld_p1 string) {
	e.Lock()
	defer e.Unlock()
	blackListCounter.With(prometheus.Labels{"etld_p1": etld_p1}).Inc()
}

//func (e * DnsguardMetrics) RecordCurlError(vendor string) {
//	curlErrorCollector.With(prometheus.Labels{"vendor": vendor}).Inc()
//}
//
//

//
//
//func  (e * DnsguardMetrics) RecordComputeHashDuration ( code int) { // write a
//
//	start := time.Now()
//	duration := time.Since(start)
//
//	histogram.WithLabelValues(fmt.Sprintf("%d", code)).Observe(duration.Seconds())
//
//}
