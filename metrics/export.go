package metrics

/*

	push metrics to a prometheus pushgateway


	Exporter

	periodically read metrics on internal server ( localhost:3333/metrics and send it to a prometheus gateway )

	usage:
		exporter := metrics.NewExporter(
			"http://localhost:3333/api/v1/agents/demo/config",
			"http://192.168.99.100:9091/metrics/job/dnsguard/instance/demo",
			"dnsguard,
			"demo"
			)
		exporter.Run(context.Background(), 5)


	see: https://zpjiang.me/2017/07/10/Push-based-monitoring-for-Prometheus/

	# Send metrics to pushgateway
	curl -s http://$EXPORTER_ADDR/$EXPORTER_METRIC |
	curl --data-binary @- http://$PGW_ADDR/metrics/job/$PGW_JOB/instance/$PGW_INSTANCE

*/

import (
	"context"
	"log"
	"net/http"
	"sync"
	"time"
)

//  compute push gateway url to push prometheus metrics
// eg: http://192.168.99.100:9091/metrics/job/dnsguard/instance/demo
func PushGatewayUrl(host, job, instance string) string {

	gatewayUrl := "http://" + host + "/metrics/job/" + job + "/instance/" + instance
	return gatewayUrl
}

type Exporter struct {
	sync.Mutex

	Source string // url to grab prometheus metrics ( localhost:3333/metrics

	Gateway string // url of the prometheus gateway to push result (http://<host>:9091/metrics/job/dnsguard/instance/demo

	Instance string // name of the prometheus instance  ( ProbeId )
	Job      string // name of the prometheus job   ( dnsguard )

	Period int // time between 2 grabs
}

func NewExporter(source, gateway, instance, job string) *Exporter {

	if job == "" {
		job = "dnsguard"
	}

	exporter := &Exporter{
		Source: source, Gateway: gateway,
		Instance: instance, Job: job,
		Period: 5,
	}

	return exporter
}

func (e *Exporter) Run(ctx context.Context, period int) {

	if period > 0 {
		e.Period = period
	}

	log.Printf("Start prometheus exporter from %s ,to %s ,with period: %d second(s)\n", e.Source, e.Gateway, e.Period)

	for {

		// fetch metrics from local and send it to gateway
		e.RunOnce()

		// wait
		time.Sleep(time.Duration(e.Period) * time.Microsecond)

	}
}

func (e *Exporter) RunOnce() {

	// read metrics from local source  (localhost:3333/metrics )
	// and send it to gateway

	var netClient = &http.Client{
		Timeout: time.Second * 1,
	}

	// fetch source
	resp, err := netClient.Get(e.Source)
	if err != nil {
		log.Printf("prometheus exporter: cannot read from internal source: %s : %s\n", e.Source, err.Error())
		return
	}

	// send what we received to gateway
	//gatewayUrl = PushGatewayUrl(e.Gateway, e.Job, e.Instance)
	gatewayUrl = e.Gateway
	//gatewayUrl := "http://" + e.Gateway + "/metrics/job/" + e.Job + "/instance/" + e.Instance
	// eg http://192.168.99.100/metrics/job/dnsguard/instance/ProbeId
	_, err = netClient.Post(gatewayUrl, "text/plain", resp.Body)
	if err != nil {
		log.Printf("prometheus exporter: cannot write to gateway at: %s : %s\n", gatewayUrl, err.Error())

	} else {
		//log.Printf("prometheus exporter: push metrics gateway ok at: %s \n", gatewayUrl)

	}

}
