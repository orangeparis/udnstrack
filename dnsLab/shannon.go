package dnsLab

/*

	EXPERIMENTAL


	determine shanon entropy of a dns domain

	see: http://blog.davidvassallo.me/tag/anomaly-detection/
	and  https://gist.github.com/dvas0004/ebb2d40694cb7eeb1d9df1c1cac6f690


 */

//import (
//
//	"github.com/gonum/stat"
//)
//
//
//
//func calcShannon(data []uint8) float64 {
//	var entropy = float64(0);
//	var histogram = make([]float64, 256);
//	var total = 0.0
//
//	for i := range data {
//		histogram[data[i]]++
//		total++
//	}
//
//	for j := range histogram {
//		histogram[j]=histogram[j]/total
//	}
//
//	entropy = stat.Entropy(histogram)
//	return entropy;
//}