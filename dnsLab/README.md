a package to test/tune dnswatch parameters


# goal

compute parameter values for threshold 
to distinguish DGA domains from normal domains


# features

## bigram frequency table

build a bigram frequency table from the 100 000 first entries of the alexa list


## normal domains

compute and chart the score of rhe 10 000 first normal domains


## tunnels

compute and chart the score of generated domains ( denise tunnel)


