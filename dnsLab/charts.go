package dnsLab





import (


	"github.com/agonopol/gosplat"
	"time"
	"log"
	"bitbucket.org/orangeparis/udnstrack/bigrams"
)





func Linechart() {
	//Create a frame to put charts in
	f := gosplat.NewFrame("Linechart Example Frame")

	//Create a chart
	v := gosplat.NewChart()

	//Add some random data
	v.Append(map[string]interface{}{"date": "2011/07/23", "thing": 10, "thong": 20, "whatevs": 14})
	v.Append(map[string]interface{}{"date": "2011/07/24", "thing": 12, "thong": 24, "whatevs": 24})
	v.Append(map[string]interface{}{"date": "2011/07/24", "thing": 12, "thong": 7, "whatevs": 11})

	//Add the chart to the Frame
	f.Append("Linechart Example", v.Linechart())

	//Preview generates a tmp html file and opens it with the default browser
	err := f.Preview()
	if err != nil {
		panic(err)
	}

	//Html returns bytes.Buffer of the html
	buffer, err := f.Html()
	if err != nil {
		panic(err)
	}
	println(buffer.String())
}



func FrequencyChart() {


	start := time.Now()
	index, _ := bigrams.NewBigramIndex()
	// load 100 000 entries
	err := bigrams.LoadBigramsFromFile(index, top_count)
	if err != nil {
		log.Fatal(err)
	}
	elapsed := time.Since(start)
	log.Printf("build bigram for %d entries took %s", top_count, elapsed)



	tbl := bigrams.NewFrequencyTable("Top 100 000 domains from Alexa list", index)


	//Create a frame to put charts in
	f := gosplat.NewFrame("Frequency Chart")

	//Create a chart
	v := gosplat.NewChart()
	v2 := gosplat.NewChart()


	for i,bigram := range tbl.ByRank() {

		v.Append(map[string]interface{} {  "occurence": tbl.Occurence(bigram) , "rank": i    })
		v2.Append(map[string]interface{}{"rank": i, "frequency": tbl.Frequency(bigram)})

	}

	//Add the chart to the Frame
	//f.Append("bigram occurence by rank", v.Linechart())


	//f.Append("bigram occurences", v.Combochart(map[string]interface{}{
	//	"title":      "bigram occurences by rank",
	//	"vAxis":      map[string]interface{}{"title": "occurence"},
	//	"hAxis":      map[string]interface{}{"title": "rank"},
	//	"seriesType": "bars",
	//	"series":     map[string]interface{}{"2": map[string]interface{}{"type": "line"}}}))

	f.Append("bygram frequency", v2.Combochart(map[string]interface{}{
		"title":      "bigram frequency by rank",
		"vAxis":      map[string]interface{}{"title": "frequency"},
		"hAxis":      map[string]interface{}{"title": "rank"},
		"seriesType": "bars",
		"series":     map[string]interface{}{"1": map[string]interface{}{"type": "line"}}}))



	//Preview generates a tmp html file and opens it with the default browser
	err = f.Preview()
	if err != nil {
		panic(err)
	}

	//Html returns bytes.Buffer of the html
	buffer, err := f.Html()
	if err != nil {
		panic(err)
	}
	println(buffer.String())
}