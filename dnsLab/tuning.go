package dnsLab


import (

	"os"
	"time"
	"log"
	"bitbucket.org/orangeparis/udnstrack/bigrams"
	//"container/heap"
	"bufio"
	"strings"
	"fmt"

	"container/heap"
)





var (

	top_source = "../assets/top-1m.csv"
	top_count = 100000
)


func TuneNormalDomains( size int ) (err error){

	/*

		build a table frequency for the 100 000 alexa domains


		for each domain of the alexa 10 000

		compute the score
		retain the result on a heap stack


	 */


	if size == 0 {
		size = 100000
	}


	// build frequency table
	start := time.Now()
	index, _ := bigrams.NewBigramIndex()
	// load 100 000 entries
	err = bigrams.LoadBigramsFromFile(index, top_count)
	if err != nil {
		log.Fatal(err)
	}
	elapsed := time.Since(start)
	log.Printf("build bigram for %d entries took %s", top_count, elapsed)

	tbl := bigrams.NewFrequencyTable("Top 100 000 domains from Alexa list", index)


	f1 := tbl.Frequency("go")
	_=f1



	// evaluate the 100 first alexa domains
	nb_domains := 10000


	// init priority queue
	pq := make(bigrams.PriorityQueue, nb_domains)
	_=pq


	domains,err := IterDomains("",nb_domains)
	if err != nil { return err }


	repo := bigrams.NewDomainStats("100 domains of alexa",domains,tbl)

	repo.Compute(pq)

	//i := 0
	//for value, priority := range bigrams.Index {
	//	pq[i] = &Item{
	//		value:    value,
	//		priority: priority,
	//		index:    i,
	//	}
	//	i++
	//}
	heap.Init(&pq)


	for pq.Len() > 0 {
		item := heap.Pop(&pq).(*bigrams.Item)
		fmt.Printf("%.2d:%s \n", item.Priority, item.Value)
	}



	return err

}







func  IterDomains (filename string, limit int)  (<-chan string,error ) {

	/*

		build a domain generator over a file

	 */

	ch := make(chan string)

	if limit == 0 {
		limit = top_count
	}
	if filename == "" {
		filename = top_source
	}


	file, err := os.Open(filename)
	if err != nil {
		return ch,err
	}

	go func () {

		defer file.Close()

		counter := 0
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			line := scanner.Text()
			//fmt.Println(line)
			counter += 1
			if counter > limit {
				close(ch)
				break
			}
			// split indice / domain name
			s := strings.Split(line, ",")
			domain :=""
			if len(s) >= 2 {
				domain = s[1]
			} else {
				domain = s[0]
			}

			// add domain to channel
			ch <- domain
			}

	}()
	return ch,err

}


func TuneEcryptedlDomains( size int ) (err error){

	/*

		build a table frequency for the 100 000 alexa domains


		for each domain of the alexa 10 000

		compute the score
		retain the result on a heap stack


	 */


	if size == 0 {
		size = 10000
	}


	// build frequency table
	start := time.Now()
	index, _ := bigrams.NewBigramIndex()
	// load 100 000 entries
	err = bigrams.LoadBigramsFromFile(index, top_count)
	if err != nil {
		log.Fatal(err)
	}
	elapsed := time.Since(start)
	log.Printf("build bigram for %d entries took %s", top_count, elapsed)

	tbl := bigrams.NewFrequencyTable("Top 100 000 domains from Alexa list", index)


	f1 := tbl.Frequency("go")
	_=f1



	// evaluate the 100 first encrypted domains
	nb_domains := 1000


	// init priority queue
	pq := make(bigrams.PriorityQueue, nb_domains)
	_=pq


	domains,err := IterDomains("../assets/encrypted_domains.txt",nb_domains)
	if err != nil { return err }


	repo := bigrams.NewDomainStats("100 encrypted domains",domains,tbl)

	repo.Compute(pq)

	//i := 0
	//for value, priority := range bigrams.Index {
	//	pq[i] = &Item{
	//		value:    value,
	//		priority: priority,
	//		index:    i,
	//	}
	//	i++
	//}
	heap.Init(&pq)


	for pq.Len() > 0 {
		item := heap.Pop(&pq).(*bigrams.Item)
		fmt.Printf("%.2d:%s \n", item.Priority, item.Value)
	}



	return err

}