package dnsLab




import (

	"os"
	"log"
	"strings"
	"testing"
	"bufio"
	"golang.org/x/net/publicsuffix"

)



func TestTuneNormalDomains ( t *testing.T) {


	TuneNormalDomains(0)



}


func TestEncrytedlDomains ( t *testing.T) {


	TuneEcryptedlDomains(0)

}



func TestEtldPlusOne ( t *testing.T) {

	/*
		test the etldPlusOne function on alexa domains ans show errors


	 */


	limit:=  100000

	errs := 1

	file, err := os.Open(top_source)
	if err != nil {
		log.Fatal(err)
		//return err
	}
	defer file.Close()

	counter := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		//fmt.Println(line)
		counter += 1
		if counter > limit {
			break
		}
		// split indice / domain name
		s := strings.Split(line, ",")
		if len(s) >= 2 {
			domain := s[1]
			//fmt.Println(domain)


			// compute etldPlusOne
			eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(domain, "."))
			if err != nil {

				errs += 1
				log.Printf("%s\n",err.Error())
				//mean_domain = domain
			} else {
				if len(domain) > len(eTLDPlusOne) {
					// the domain is longer than
					//mean_domain = domain[: len(domain)-len(eTLDPlusOne)-1] + ".nul"

				}
			}

		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}


	pct := float64(errs)/float64(limit) * 100
	log.Printf("errors : %d for %d domains  (%f %%)\n", errs,limit,pct)



}