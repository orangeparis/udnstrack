package dnsadmin

import (
	"fmt"
	"net/http"
	"sync"
)

func (s *server) handleIndex() http.HandlerFunc {
	//thing := prepareThing()
	return func(w http.ResponseWriter, r *http.Request) {
		// use thing
		fmt.Fprintf(w, "%s\n", "Welcome to dnsadmin home page")

	}
}

func (s *server) handleSomething() http.HandlerFunc {

	//thing := prepareThing()

	type request struct {
		Name string
	}
	type response struct {
		Greeting string `json:"greeting"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		// use thing
	}
}

func (s *server) handleGreeting(format string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, format, "World")
	}
}

func (s *server) handleTemplate(files ...string) http.HandlerFunc {
	var (
		init sync.Once
		//tpl  *template.Template
		err error
	)
	return func(w http.ResponseWriter, r *http.Request) {
		init.Do(func() {
			//tpl, err = template.ParseFiles(files...)
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		// use tpl
	}
}
