package proxy

/*

	master a client to master ( configuration )

*/

import (
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"
)

/*

 updater periodically poll backend for config and update local config and database ( whitelist / blacklist )

 fetch config from backend
 	example

 			GET https://agentconnectorint.kmt.orange.com/api/v1/agents/DEMO/config

 			headers   Authorization: Basic aHlwZXJhZG1pbjpoYWRlZmF1bHRwd2Q=

	assuming Demo is the ProbeId


	update local redis with config


*/

var (

	// default timeout to fetch backend config
	default_fetch_timeout = 2 * time.Second

	//
	default_period = "24h"

	// ttl for storing whitelist blacklist
	default_store_ttl = uint64(3600 * 24)
)

type Updater struct {

	// updater is an http.Client
	http.Client
	sync sync.Mutex

	Host          string // backend host ( eg localhost:3333 )
	Authorization string // backend Authorization header

	ProbeId string // name of the prometheus instance  ( ProbeId )

	Period time.Duration // time between 2 grabs

	Store *dnsbroker.Store // redis store
}

func NewUpdater(host string, authorization string, probeId string, period int, store *dnsbroker.Store) (updater *Updater) {

	delay := time.Duration(period) * time.Second

	updater = &Updater{
		Host:          host,
		Authorization: authorization,
		ProbeId:       probeId,
		Period:        delay,
		Store:         store,
	}

	// set a timeout for the built in http client
	updater.Timeout = default_fetch_timeout

	return updater
}

func (u *Updater) Fetch() (data *BackendConfig, err error) {
	//
	//  fetch the backend config ( eg GET http://<host>/api/v1/agents/demo/config
	//  fetch source
	https := false
	if u.Authorization != "" {
		https = true
	}
	url := u.FetchUrl(https)

	resp, err := u.Get(url)

	if err != nil {
		msg := fmt.Sprintf("updater: cannot fetch backend config at %s : %s\n", url, err.Error())
		log.Println(msg)
		err = errors.New(msg)
		return data, err
	}
	if resp.StatusCode == 200 {
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {

			data, err = NewBackendConfig(body)
			if err != nil {
				log.Printf("cannot convert backend config: %s", err.Error())
				return data, err
			}
		}
	} else {
		msg := fmt.Sprintf("updater: failed fetching backend config at %s : %s\n", url, err.Error())
		log.Println(msg)
		err = errors.New(msg)
		return data, err
	}

	return data, err
}

func (u *Updater) Update(data *BackendConfig) (err error) {
	//

	err = data.UpdateStore(u.Store, default_store_ttl)

	return err
}

func (u *Updater) FetchUrl(https bool) (url string) {
	// return the fetch url ( get config from the backend )

	if https == false {
		// no https
		url = "http://" + u.Host + "/api/v1/agents/" + u.ProbeId + "/config"
	} else {
		url = "https://" + u.Host + "/api/v1/agents/" + u.ProbeId + "/config"
	}
	// eg http://192.168.99.100:8080/api/v1/agents/demo/config

	return url
}

func (u *Updater) RunOnce() {

	// fetch configuration from backend

	data, err := u.Fetch()
	if err == nil {
		// update configuration
		u.Update(data)

	}

}

func (u *Updater) Run(ctx context.Context) {

	for {

		https := false
		if u.Authorization != "" {
			https = true
		}

		log.Printf("Udating config from backend at: %s\n", u.FetchUrl(https))
		u.RunOnce()

		time.Sleep(u.Period)

	}
}
