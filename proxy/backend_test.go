package proxy_test

import (
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"bitbucket.org/orangeparis/udnstrack/proxy"
	"io/ioutil"
	"log"
	"testing"
)

var (
	sample_config = "../tests/rules.json"
)

func TestDecodeConfig(t *testing.T) {

	// read a sample backend config file ( rules.json )
	data, err := ioutil.ReadFile(sample_config)
	if err != nil {
		log.Printf("cannot read sample config:%s", sample_config)
		return
	}

	backend, err := proxy.NewBackendConfig(data)
	if err != nil {
		log.Printf("cannot convert backend config: %s", err.Error())
		return
	}

	_ = backend

	// database update (redis)
	//backend.UpdateBlacklist()
	//backend.UpdateWhitelist()
	//backend.UpdateRules()

	println("Done")

}

func TestUpdateConfig(t *testing.T) {

	// read a sample backend config file ( rules.json )
	data, err := ioutil.ReadFile(sample_config)
	if err != nil {
		log.Printf("cannot read sample config:%s", sample_config)
		return
	}

	backend, err := proxy.NewBackendConfig(data)
	if err != nil {
		log.Printf("cannot convert backend config: %s", err.Error())
		return
	}

	_ = backend

	//
	// update local config with backend config
	//
	local := &dnsbroker.DnstrackConfig{}
	err = backend.UpdateLocal(local)

	//
	// update redis db with backend config
	//
	// create store
	dnsbroker.NewHive("test", "redis:localhost")
	store := dnsbroker.NewStore()

	delay := uint64(30)

	// database update (redis)
	err = backend.UpdateStore(store, delay)
	//backend.UpdateBlacklist()
	//backend.UpdateWhitelist()
	//backend.UpdateRules()

	println("Done")

}
