package proxy

/*

	the backend config type

	we get a rules.json file from the backend  ( see doc/backend.md for structure )

	and we update the config with it


*/

import (
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"encoding/json"
	"fmt"
	"github.com/davecgh/go-spew/spew"
)

type BackendConfig struct {
	// format of the remote backed configuration

	Config BackendMain `json:"config"`

	Blacklist []string `json:"blacklist"`
	Whitelist []string `json:"whitelist"`

	Rules []BackendRule `json:"rules"`
}

type BackendMain struct {
	Get_config_frequency            int  `json:"get_config_frequency"`            // 300
	Simulation_mode                 bool `json:"simulation_mode"`                 // false
	Push_detected_domains_frequency int  `json:"push_detected_domains_frequency"` //  60

}

type BackendRule struct {
	Field_label string `json:"field_label"` //	"TXT"
	Rule_label  string `json:"rule_label"`  //  "txt_rule"
	Max_length  int    `json:"max_length"`  //	 0

}

func NewBackendConfig(backend_config []byte) (*BackendConfig, error) {

	backend := &BackendConfig{}
	err := json.Unmarshal(backend_config, backend)

	return backend, err

}

func (c *BackendConfig) UpdateLocal(local *dnsbroker.DnstrackConfig) (err error) {

	// update the local config  with the backend config

	// SimulationMode -> local.DryMode
	local.DryMode = c.Config.Simulation_mode

	// Get_config_Frequency ->
	local.Backend.PollConfigFrequency = c.Config.Get_config_frequency

	// Push_detected_domains_frequency -> deprecated
	_ = c.Config.Push_detected_domains_frequency

	return err
}

func (c *BackendConfig) UpdateStore(store *dnsbroker.Store, ttl uint64) (err error) {

	// no tll on whitelist
	var whitelist_ttl uint64 = 0

	spew.Dump(c)

	err = c.UpdateBlacklist(store, ttl)
	err = c.UpdateWhitelist(store, whitelist_ttl)
	err = c.UpdateRules(store, ttl)

	return err
}

func (c *BackendConfig) UpdateBlacklist(store *dnsbroker.Store, ttl uint64) (err error) {

	// update blacklist
	for _, v := range c.Blacklist {
		fmt.Printf("bl: %s\n", v)
		err = store.AddBlacklist(v, ttl)
		if err != nil {
			break
		}
	}
	return err
}

func (c *BackendConfig) UpdateWhitelist(store *dnsbroker.Store, ttl uint64) (err error) {

	// update whitelist
	for _, v := range c.Whitelist {
		fmt.Printf("wl: %s\n", v)
		err = store.AddWhitelist(v, ttl)
		if err != nil {
			break
		}
	}

	return err
}

func (c *BackendConfig) UpdateRules(store *dnsbroker.Store, ttl uint64) (err error) {

	// update blacklist
	for _, rule := range c.Rules {

		fmt.Printf("rule: %s\n", rule.Field_label)
		//size,err := rule.Max_length
		if err == nil {
			err = store.SetRule(rule.Field_label, string(rule.Max_length), ttl)
			if err != nil {
				break
			}
		}
	}

	return err
}
