package udnswatch


import (
	"os"
	"log"
	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	//"time"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"strings"
	"time"
)



const DEFAULT_SCAN_LIMIT = 40
const DEFAULT_SCAN_PERIOD = 60
const DEFAULT_BLACKLIST_TTL=  3600
const DEFAULT_WHITELIST_TTL=  3600


//
const SCAN_PERIOD_RATIO int = 3


// a redis pubsub publisher
type Watcher struct {

	store * dnsbroker.Store
	conf * dnsbroker.DnstrackConfig
}


func NewWatcher() *Watcher {


	log.Printf("create new watcher\n")


	// get a copy of the config
	conf := dnsbroker.GetConfig()
	fmt.Printf("version: %s\n", conf.Version)
	spew.Dump(conf)

	// get a Store client
	store := dnsbroker.NewStore()

	watcher := Watcher{store,conf}


	//onInterruptSignal(func() {
	//	guardian.Close()
	//	//nfqueue.Close()
	//})


	return &watcher
}


func (watch * Watcher) Close() {
	//
	log.Printf("watcher closed")
	os.Exit(-1)
}


func (watch * Watcher) WatchDomainCounter() {
	//
	//  iter on domain counters (hit:*) and blacllist domain if counter exeed a limit
	//
	var err error

	// load configuration SCAN_LIMIT
	limit := watch.conf.ScanLimit
	if limit == 0 {
		limit = DEFAULT_SCAN_LIMIT
	}

	log.Printf("enter watcher cycle")

	// iter domain counter keys and blacklist domains where counters > LIMIT
	counter_keys, err := watch.store.ListDomainCounter()
	if err == nil {
		for _, domain := range counter_keys {

			counter := watch.store.GetDomainCounter(domain)
			//log.Printf("count for %s :%d", domain, counter)
			if counter >= limit {
				// limit is hit: set_blacklist
				watch.BlacklistDomain(domain)
			}

		}
	}
	log.Printf("exit watcher cycle")
}




func (watch * Watcher) Run() {
	//

	// load SCAN_PERIOD from conf
	period := time.Duration(watch.conf.ScanPeriod)
	if period == 0 {
		period = time.Duration(DEFAULT_SCAN_PERIOD)
	}


	log.Printf("enter watcher loop")


	// main loop
	for {


		// split the period in 3 part
		sub_period := time.Duration(int(period) / SCAN_PERIOD_RATIO)


		for i := 1; i <= SCAN_PERIOD_RATIO; i++ {

			log.Printf("enter sub period %d" ,i)
			time.Sleep( sub_period * time.Second)

			// main action survey counters for blacklist
			log.Printf("watch counters")
			watch.WatchDomainCounter()
			log.Printf("leave sub period %d" ,i)
		}



		// set duration of period
		//time.Sleep( period * time.Second)
		// main action survey counters for blacklist
		//watch.WatchDomainCounter()


		// cooldown tempo
		time.Sleep(1 *  time.Millisecond)


		// reset counters
		watch.ResetDomainCounter()

	}



	log.Printf("exit watcher loop")
	watch.Close()
}


func (watch * Watcher) ResetDomainCounter() {
	//
	// remove domain counters (hit:*) for a fresh start
	var err error
	log.Printf("enter watcher cycle")

	// iter domain counter keys and blacklist domains where counters > LIMIT
	counters, err := watch.store.ListDomainCounter()
	if err == nil {
		for _, domain := range counters {
			//log.Printf("clear domain counter for ", domain )
			watch.store.ClearDomainCounter(domain)

		}
	}
}





//
// uyilities
//


func (watch * Watcher) BlacklistDomain( domain string) {
	//
	// blacklist the domain if is not in whitelist or unknown
	//

	// load BLACKLIST_TTL from conf
	ttl := watch.conf.BlacklistTTL
	if ttl == 0 {
		ttl = uint64(DEFAULT_BLACKLIST_TTL)
	}


	if domain == "unknown." {
		// the server cannot determine the etld plus one
		// so we let pass it but add to unknown list
		log.Printf("unknown domain")
		return
	}

	if watch.IsInWhitelist(domain) == false {
        // blacklist domain if not in whitelist
		watch.store.AddBlacklist(domain, ttl)
		log.Printf("blacklist domain %s with ttl %d", domain, ttl)
	}
}


func (watch * Watcher) IsInWhitelist( domain string) (bool) {
	//
	// test if domain is in whitelist
	result := true

	// load WHITELIST_TTL from conf
	ttl := watch.conf.WhitelistTTL
	if ttl == 0 {
		ttl = uint64(DEFAULT_WHITELIST_TTL)
	}

	// see if domain is stored in redis whitelist
	if  watch.store.IsInWhitelist(domain) != true{

		// see with primary whitelist
		wlist := watch.conf.WhiteList

		result = false
		for _, wdomain := range wlist {

			// see if domain starts with wdomain  eg google. or apple.
			ref := wdomain + "."
			if strings.HasPrefix(domain,ref) {
				// this is domain is in whitelist: add it to redis whitelist
				watch.store.AddWhitelist(domain,ttl)
				result= true
				break
			}
		}
	}
	return result
}


