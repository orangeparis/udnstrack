package udnswatch_test

import (

	"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	"bitbucket.org/orangeparis/udnstrack/udnswatch"

	"testing"
	"log"
	//"time"
	//"github.com/stretchr/testify/assert"
	"fmt"
	"github.com/davecgh/go-spew/spew"
)

var HIVE_NAME = "hive"
//var REDIS_URL = "redis://localhost:6379/0"
//var REDIS_URL = "redis://192.168.99.100:6379/0"
var REDIS_URL = "redis://redis:6379/0"

func newHive(url string) * dnsbroker.Hive {

	name := HIVE_NAME

	if url == "" {
		url = REDIS_URL
	}
	dnsbroker.ResetHive()

	h := dnsbroker.NewHive(name,url)
	hive := dnsbroker.GetHive()

	println(hive==h)

	return hive

}




func TestUdnswatchCore(t *testing.T) {


	url := REDIS_URL


	// reset hive
	hive := newHive(url)

	// create new config
	config := dnsbroker.NewConfig("./dnstrack.toml")
	fmt.Printf("version: %s\n", config.Version)
	spew.Dump(config)


	//// get a watcher instance
	//store := dnsbroker.NewStore()

	watcher := udnswatch.NewWatcher()

	// watcher.WatchDomainCounter()

	//watcher.ResetDomainCounter()

	watcher.Run()

	log.Printf("%s",watcher)







	hive.Close()
}

