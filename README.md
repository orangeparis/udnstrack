udnstrack
=========

a standalone dns server solution which detects and blocks dns tunnels

see doc/notes.md for details


prerequisites
-------------
any linux machine with 

* git 
* docker 
* docker-compose 

access to a docker repository providing official base images (or dockerhub.com)

* golang:alpine
* alpine:edge

access to this git repository

    https://bitbucket.org/orangeparis/udnstrack.git

installation
============

check docker
------------
    docker -v
    docker pull golang:alpine
    docker pull alpine:edge

install this repository if not done.
------------------------------------
    mkdir /app
    cd /app
    git clone https://bitbucket.org/orangeparis/udnstrack.git udnstrack

configure udnsguard
-------------------
edit the file 

    /app/udnstrack/docker/dnsguardian/dnsguard/src/dnstrack.toml

and customize values for

    # time to live for a blacklisted domain
    blacklist_ttl = 300  #  5 mn
    
    # duration in s of the scan to find tunnels
    scan_period = 60 # seconds
    
    # limit of unique hostname for a domain to detect a tunnel over the scan_period
    scan_limit = 40

    #
    # Backend configuration 
    #
    [backend]
    # set the key or probe id
    ProbeId = "DEMO"
    # set the backend host  ( eg agentconnectorint.kmt.orange.com )
    Master = "localhost:3333"
    # set the authentication token  ( for https basic authentication )
    MasterToken = ""   
    # set the prometheus pushgateway url
    PrometheusUrl = "http://192.168.99.100:9091/metrics/job/dnsguard/instance/demo"


    # declare the whitelist here ( domain root that cannot be blacklisted )
    whitelist= [
        "google",
        "apple",
        "tmall"
    ]

    



configure the unbound dns server (if necessary)
-----------------------------------------------
edit the file

    /app/udnstrack/docker/dnsguardian/dns/unbound.conf
    
see https://calomel.org/unbound_dns.html for a unbound tutorial

build the local images
----------------------
cocoon/dns

    cd /app/udnstrack/docker/images/dns
    ./build
    
cocoon/basedns

    cd /app/udnstrack/docker/images/gobase
    ./build

build the udnstrack solution
----------------------------
    cd /app/udnstrack/docker/dnsguardian
    docker-compose build
    
    
start the tracker
-----------------
    cd /app/udnstrack/docker/dnsguardian
    docker-compose up
    
        
stop the tracker
----------------
    cd /app/udnstrack/docker/dnsguardian
    docker-compose down


update the tracker (need access to the udnstrack repository)
------------------

update the udnstrack repository

    cd /app/udnstrack/docker/dnsguardian
    git pull
    
stop and clear the server

    docker-compose down --rmi local
    
restart the server

    docker-compose up

